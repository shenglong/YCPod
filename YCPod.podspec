#
# Be sure to run `pod lib lint YCPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "YCPod"
  s.version          = "0.3.3"
  s.summary          = "Sheng Long XM Pod YC."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                       Sheng Long XM Pod YC.
                       Base.
                        DESC

  s.homepage         = "https://git.oschina.net/shenglong"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "shenglong" => "shenglong514@gmail.com" }
  s.source           = { :git => "https://git.oschina.net/shenglong/YCPod.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  #s.source_files = 'Pod/Classes/**/*'
  #s.resource_bundles = {
  #    'YCPod' => ['Pod/Assets/*.png']
  # }

    s.subspec 'CustomView' do |view|
        view.source_files = 'Pod/Classes/CustomView/**/*'
        view.public_header_files = 'Pod/Classes/CustomView/**/*.h'
        view.resources = 'Pod/Assets/CustomView/*.png'
        view.dependency 'YCPod/CommUtls'
        view.dependency 'YCPod/Base'
        view.dependency 'ReactiveCocoa', '~> 2.5'
        view.dependency 'Masonry', '>= 0.6.2'
    end

    s.subspec 'NetWork' do |network|
        network.source_files = 'Pod/Classes/Network/**/*'
        network.public_header_files = 'Pod/Classes/Network/**/*.h'
        network.dependency 'YCPod/CommUtls'
        network.dependency 'AFNetworking', '>= 2.3'
        network.dependency 'ReactiveCocoa', '~> 2.5'
        network.dependency 'Reachability', '~> 3.2'
    end

    s.subspec 'Base' do |base|
        base.source_files = 'Pod/Classes/Base/**/*'
        base.public_header_files = 'Pod/Classes/Base/**/*.h'
        base.dependency 'ReactiveCocoa', '~> 2.5'
        base.dependency 'Mantle', '~> 1.5.5'
        base.dependency 'Masonry', '>= 0.6.2'
        base.dependency 'FMDB', '~> 2.5'
        base.dependency 'LKDBHelper', '~> 2.1.2'
        base.dependency 'IQKeyboardManager', '~> 3.2.4'
        base.dependency 'YCPod/NetWork'
    end

    s.subspec 'CommUtls' do |utls|
        utls.source_files = 'Pod/Classes/CommUtls/**/*'
        utls.public_header_files = 'Pod/Classes/CommUtls/**/*.h'
        utls.dependency 'OpenUDID', '~> 1.0.0'
        utls.dependency 'Reachability', '~> 3.2'
        utls.frameworks = 'CoreTelephony', 'SystemConfiguration'
    end

    s.subspec 'LoadingView' do |loadingView|
        loadingView.source_files = 'Pod/Classes/LoadingView/**/*'
        loadingView.public_header_files = 'Pod/Classes/LoadingView/**/*.h'
        loadingView.resources = 'Pod/Assets/LoadingView/*.png'
        loadingView.dependency 'YCPod/CommUtls'
        loadingView.dependency 'Masonry', '>= 0.6.2'
        loadingView.dependency 'ReactiveCocoa', '~> 2.5'
    end

    s.subspec 'AutoRefreshTable' do |table|
        table.source_files = 'Pod/Classes/AutoRefreshTable/**/*'
        table.public_header_files = 'Pod/Classes/AutoRefreshTable/**/*.h'
        table.resources = 'Pod/Assets/AutoRefreshTable/*.png'
        table.dependency 'YCPod/LoadingView'
        table.dependency 'YCPod/Base'
    end

    s.source_files = 'Pod/Classes/YCPod.h'
    s.public_header_files = 'Pod/Classes/YCPod.h'

    s.frameworks = 'UIKit', 'Foundation'
end
