# YCPod

[![CI Status](http://img.shields.io/travis/shenglong/YCPod.svg?style=flat)](https://travis-ci.org/shenglong/YCPod)
[![Version](https://img.shields.io/cocoapods/v/YCPod.svg?style=flat)](http://cocoapods.org/pods/YCPod)
[![License](https://img.shields.io/cocoapods/l/YCPod.svg?style=flat)](http://cocoapods.org/pods/YCPod)
[![Platform](https://img.shields.io/cocoapods/p/YCPod.svg?style=flat)](http://cocoapods.org/pods/YCPod)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YCPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YCPod"
```

## Author

shenglong, 45024798@qq.com

## License

YCPod is available under the MIT license. See the LICENSE file for more info.
