//
//  CommUtls.h
//  UtlBox
//
//  Created by cdel cyx on 12-7-10.
//  Copyright (c) 2012年 cdeledu. All rights reserved.
//

/*
 framework   UIKit  CoreGraphics SystemConfiguration
 */
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CommUtls : NSObject

/**
 *  获取颜色值，将16进制字符转换成UIColor
 *
 *  @param stringToConvert <#stringToConvert description#>
 *
 *  @return <#return value description#>
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

/**
 *  将UIColor颜色转换成16进制字符
 *
 *  @param color <#color description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)changeUIColorToRGB:(UIColor *)color;

/**
 *  将16进制UIColor字符转换成rgb色值
 *
 *  @param color <#color description#>
 *
 *  @return <#return value description#>
 */
+ (NSArray *)getSelectedColor:(NSString *)color;


/**
 *  获取UILabel高度
 *
 *  @param content  内容
 *  @param font     字体
 *  @param size     内容大小
 *  @param kernGap  字体左右间隔
 *  @param paragGap 行间距
 *
 *  @return
 */
+ (CGSize)returnContentSize:(NSString *)content font:(UIFont *)font width:(CGFloat)width;
+ (CGSize)returnContentSize:(NSString *)content font:(UIFont *)font size:(CGSize)size;
+ (CGSize)returnContentSize:(NSString *)content font:(UIFont *)font size:(CGSize)size kernGap:(CGFloat)kernGap paragGap:(CGFloat)paragGap;

/**
 *  复制一个对象
 */
+ (id)duplicateView:(id)object;

@end
