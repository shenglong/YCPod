//
//  CustomTextField.h
//  PhoneApp
//
//  Created by Sheng long on 15-3-7.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCCustomTextField : UITextField

@property (nonatomic,strong) UIColor *placeholderColor;

@end
