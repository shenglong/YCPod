//
//  CustomUITextView.h
//  CPACommunity
//  自定义UITextView
//  Created by Mac on 13-6-1.
//  Copyright (c) 2013年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCCustomUITextView : UITextView<UITextViewDelegate>

/**
 *  设置提示语言属性
 */
@property (nonatomic,strong) NSString *tip;

@end
