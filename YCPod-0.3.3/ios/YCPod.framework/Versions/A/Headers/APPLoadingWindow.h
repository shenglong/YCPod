//
//  TKLoadingView.h
//  MobileSchool
//
//  Created by SL on 14/11/12.
//  Copyright (c) 2014年 feng zhanbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APPLoadingWindow : UIView

//
-(void)showToolTip:(NSString *)title
          interval:(NSTimeInterval)time
            inView:(UIView *)view;

//
-(void)showLoading:(NSString *)title
             close:(void(^)())close
            inView:(UIView *)view;

@end
