//
//  CommUtls+NSString.h
//  Pods
//
//  Created by SL on 16/1/15.
//
//

#import "CommUtls.h"

@interface CommUtls (NSString)

/**
 *  验证字符个数
 *
 *  @param text 字符，会过滤前后的空格
 *  @param min  最小个数
 *  @param max  最大个数
 *
 *  @return
 */
+ (BOOL)verificationNumber:(NSString *)text
                       min:(NSInteger)min
                       max:(NSInteger)max;

/**
 *  判断手机号是否正确
 *
 *  @param phone 手机号
 *
 *  @return 
 */
+ (BOOL)getPhoneFormat:(NSString*)phone;

/**
 *  去除左右空格
 */
+ (NSString *)deleteSpace:(NSString *)text;

/**
 *  获取自定义字符NSAttributedString
 *
 *  @param titles 自定义字段
 *  @param fonts  字体
 *  @param colors 颜色
 *
 *  @return 
 */
+ (NSAttributedString *)fetchCustomString:(NSArray *)titles
                                    fonts:(NSArray *)fonts
                                   colors:(NSArray *)colors;

@end
