//
//  YCViewConstant.h
//  Pods
//
//  Created by SL on 15/12/8.
//
//

#import <Foundation/Foundation.h>

@interface YCViewConstant : NSObject

+ (instancetype)sharedInstance;

/**
 *  加载框图片，优先级别最高
 */
@property (nonatomic,strong) UIImage *loadingImg;

/**
 *  加载框color
 */
@property (atomic,strong) UIColor *loadingColor;

/**
 *  系统加载框动画
 */
@property (atomic,assign) BOOL activityAnimation;

/**
 *  加载失败重新加载
 */
@property (atomic,strong) NSString *cycleImageName;
@property (atomic,strong) NSString *cycleButtonImageName;
@property (atomic,strong) NSString *cycleButtonTip;
//有时不设置这个值的时候，title显示会居下
@property (atomic) UIEdgeInsets cycleButtontitleEdgeInsets;


#pragma mark - 下拉刷新
/**
 *  自定义下拉刷新箭头
 */
@property (atomic,strong) NSString *refreshArrowImageName;

/**
 *  自动刷新table是否显示刷新时间
 */
@property (nonatomic,assign) BOOL autoTableShowTime;

@end
