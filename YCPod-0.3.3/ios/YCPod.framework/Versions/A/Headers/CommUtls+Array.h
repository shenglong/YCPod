//
//  CommUtls+Array.h
//  Pods
//
//  Created by SL on 15/12/24.
//
//

#import "CommUtls.h"

@interface CommUtls (Array)

/**
 *  组装数组
 *  将数组array里面的所有数据，已number数量打包，返回一个新数组
 *
 *  @param array  传入的数组
 *  @param number 几个一组
 *
 *  @return 数组
 */
+ (NSArray *)assemblyArray:(NSArray *)array
                    number:(NSInteger)number;

/**
 *  根据text字段排序
 *
 *  @param array 数据源
 *  @param text  排序字段
 *  @param desc  是否降序
 *
 *  @return 排列后的数据
 */
+ (NSArray *)sequenceArray:(NSArray *)array
                      text:(NSString *)text
                      desc:(BOOL)desc;

/**
 *  根据text字段将fromArray数组的数据添加到toArray中字典的key字段里
 *
 *  @param fromArray 需要添加的数据源
 *  @param text      根据字段
 *  @param toArray   添加到的数据源
 *  @param key       添加到toArray中数据的key字段
 *
 *  @return 添加完成的数据源
 */
+ (NSArray *)packageArray:(NSArray *)fromArray
                     text:(NSString *)text
                 forArray:(NSArray *)toArray
                      key:(NSString *)key;

/**
 *  根据text字段将对象进行排序
 *
 *  @param data 数据
 *  @param text 排序字段，为该对象属性
 *  @param desc 是否降序
 *
 *  @return 排序后的数据
 */
+ (NSArray *)sequenceSort:(NSArray *)data text:(NSString *)text desc:(BOOL)desc;

@end
