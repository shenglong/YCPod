//
//  Httphelper.h
//  PhoneApp
//
//  Created by SL on 15/3/6.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface AppHttphelper : NSObject

/**
 *  GET请求
 */
+ (RACSignal *)GET:(NSString *)url
            params:(NSDictionary *)params;

/**
 *  POST请求
 */
+ (RACSignal *)POST:(NSString *)url
             params:(id)params;

/**
 *  上传图片
 *
 *  @param url         地址
 *  @param params      参数
 *  @param filePaths   文件地址，带后缀
 *  @param fileData    文件流
 *  @param fileName    文件名称，带后缀
 *
 *  @return
 */
+ (RACSignal *)UPLOAD:(NSString *)url
               params:(id)params
            filePaths:(NSArray *)filePaths
             fileData:(NSData *)fileData
             fileName:(NSString *)fileName;

/**
 *  head请求
 */
+ (RACSignal *)httpHead:(NSString *)url;

/**
 *  下载文件
 *
 *  @param aUrl     下载地址
 *  @param fileName 文件保存地址
 *  @param save     是否保存文件，否则删除
 *
 *  @return 返回一个下载完成的文件的流NSData
 */
+ (RACSignal *)downloadFileURL:(NSString *)aUrl
                      fileName:(NSString *)fileName
                          save:(BOOL)save;

///**
// *  同步请求
// */
//+ (RACSignal *)SYNC:(NSString *)url
//             params:(id)params;
//

@end
