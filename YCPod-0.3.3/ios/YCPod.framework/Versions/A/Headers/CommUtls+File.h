//
//  CommUtls+File.h
//  CdeleduUtls
//
//  Created by 陈轶翔 on 14-3-30.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

#import "CommUtls.h"

@interface CommUtls (File)

//根据文件路径删除文件
+ (BOOL)remove:(NSString *)fullPathName;

//创建文件
+ (void)makeDirs:(NSString *)dir;

//documentPath路径是否存在
+ (BOOL)fileExistInDocumentPath:(NSString*)fileName;

//删除documentPath路径下文件
+ (BOOL)deleteDocumentFile:(NSString*)fileName;

//cachePath路径是否存在
+ (BOOL)fileExistInCachesPath:(NSString*)fileName;

//返回完整的cachePath下文件路径
+ (NSString*)cachesFilePath:(NSString*)fileName;

//删除cachePath下文件路径
+ (BOOL)deleteCachesFile:(NSString*)fileName;

/**
 *	@brief	判断文件路径是否存在
 *
 *	@param 	fullPathName 	文件完整路径
 *
 *	@return	返回是否存在
 */
+ (BOOL)fileExists:(NSString *)fullPathName;

/**
 *	@brief	设置文件不被iCloud备份
 *
 *	@param 	URL 	<#URL description#>
 *
 *	@return	<#return value description#>
 */
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

/**
 *  返回完整的documentPath下文件路径
 *
 *  @param fileName 文件夹名称1/2.txt
 *
 *  @return <#return value description#>
 */
+ (NSString *)documentPath:(NSString *)fileName;

/**
 *  获取本地文件类型
 *
 *  @param path 本地文件地址
 *
 *  @return <#return value description#>
 */
+ (NSString *)mimeTypeForFileAtPath:(NSString *)path;

@end
