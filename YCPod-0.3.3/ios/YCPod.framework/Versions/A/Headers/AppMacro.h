//
//  AppMacro.h
//  PhoneApp
//
//  Created by SL on 15/3/6.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#ifndef PhoneApp_AppMacro_h
#define PhoneApp_AppMacro_h

#define IsIOS7 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0? YES : NO)
#define IsIOS8 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0? YES : NO)
#define IsIOS9 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 9.0? YES : NO)

/*
 
 网络
 
 */

#define AppNetChanged                           @"kNetStatusHelperChangedNotificationNetChanged"                   //检测网络变化的通知key

#define AppCustomErrorDomain                    @"app.error.domain"             //error.domain
#define AppErrorMsg                             @"ErrorMsg"                     //error.userInfo[key]
#define AppErrorMsg_NoNet                       @"无网络"                        //无网络
#define AppErrorMsg_DataFailed                  @"网络异常"                      //一般是访问的接口地址报404错时出现
#define AppErrorMsg_Failed                      @"获取数据失败"                   //获取数据失败显示提示

#define AppListDataNumber                       20                               //app每次获取列表条数
#define AppLoading_Again                        @"重试"                           //app获取数据失败重试默认字段

/**
 *  error.code
 */
typedef enum{
    AppDataFailed = -1000,
    AppNoNet,
    AppServiceFailed,
}CustomErrorFailed;

/**
 *  NSError默认的设置和获取
 */
#define AppErrorParsing(__error)   \
({  \
NSString *title = AppErrorMsg_Failed;  \
if ([__error isKindOfClass:[NSError class]]) {    \
if ([__error.domain isEqualToString:AppCustomErrorDomain]) {   \
title = __error.userInfo[AppErrorMsg]; \
}   \
}   \
title;  \
})

#define AppErrorSetting(__errorMsg)   \
({  \
NSString *title = AppErrorMsg_Failed;  \
if(__errorMsg){\
title = __errorMsg;\
}\
[NSError errorWithDomain:AppCustomErrorDomain code:AppDataFailed userInfo:@{AppErrorMsg:title}];\
})

/*
 
 颜色
 
 */
#import "CommUtls.h"
#define RGB(x,y,z)              [UIColor colorWithRed:x/255.0 green:y/255.0 blue:z/255.0 alpha:1.0]
#define YColor(x)               [CommUtls colorWithHexString:x]

/*
 
 常用打印，判断是否空值
 
 */

#define NodeExist(node) (node != nil && ![node isEqual:[NSNull null]])

#ifdef DEBUG
#define CLog(format, ...) NSLog(format, ## __VA_ARGS__)
//#define CLog(format, ...) do { \
//fprintf(stderr, "<%s : %d> %s\n", \
//[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], \
//__LINE__, __func__); \
//NSLog((format), ##__VA_ARGS__); \
//fprintf(stderr, "-------\n"); \
//} while (0)
#else
#define CLog(format, ...)
#endif

/*
 
 单例
 
 */

#undef	AS_SINGLETON
#define AS_SINGLETON( __class ) \
+ (__class *)sharedInstance;

#undef	DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
+ (__class *)sharedInstance \
{ \
static __class * __singleton__ = nil; \
static dispatch_once_t once; \
dispatch_once( &once, ^{ __singleton__ = [[__class alloc] init]; } ); \
return __singleton__; \
}

#endif
