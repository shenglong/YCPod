//
//  AutoListViewModel.h
//  PhoneApp
//
//  Created by Sheng long on 15/4/12.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "BaseViewModel.h"

typedef NS_ENUM(NSInteger,LSAutoRefreshTableViewModel_Signal_Type) {
    Fetch_Data_Done = 45327,            //获取数据完成
};

@interface AutoListViewModel : BaseViewModel

/**
 *  获取数据源，子类重写该方法，自定义获取数据流程
 *
 *  @param refresh 刷新/获取更多
 */
- (void)getData:(BOOL)refresh;

/**
 *  数据源
 */
@property (nonatomic,strong) NSArray *dataArray;

/**
 *  处理获取数据源的信号，父类处理接受数据源，做相应处理
 *
 *  @param signal  <#signal description#>
 *  @param refresh <#refresh description#>
 */
- (void)dealGetDataSignal:(RACSignal *)signal refresh:(BOOL)refresh;

/**
 *  获取table数据，子类重写
 */
- (NSArray *)fetchTabelData;

@end
