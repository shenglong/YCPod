//
//  BaseModel.h
//  Pods
//
//  Created by Sheng long on 16/7/4.
//
//

#import <Mantle/Mantle.h>

@protocol BaseModelDelegate <NSObject>
@required
+ (NSDictionary *)tableMapping;
@end

@interface BaseModel : MTLModel<MTLJSONSerializing>

/**
 *  转换数组对象（NSDictionarys）
 */
+ (NSArray *)modelsFromArray:(NSArray *)array;

/**
 *  转换单个对象(NSDictionary)
 */
+ (id)modelFromDictionary:(NSDictionary *)dictionary;

/**
 *  将object对象中的属性值转换成NSDictionary
 */
+ (id)properties_aps;

@end
