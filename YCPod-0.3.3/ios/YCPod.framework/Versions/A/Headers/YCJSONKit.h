//
//  NSString+YCJSONKit.h
//  YCPod
//
//  Created by Sheng long on 16/2/2.
//  Copyright © 2016年 shenglong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (YCJSONKit)

- (id)ObjectFromJSONString_yc;

@end

@interface NSData (YCJSONKit)

- (id)ObjectFromJSONData_yc;

@end

@interface NSObject (YCJSONKit)

- (NSString *)JSONString_yc;

@end
