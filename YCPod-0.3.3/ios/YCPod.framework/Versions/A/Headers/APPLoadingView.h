//
//  APPLoadingView.h
//  MobileClassPhone
//  正保加载View
//  Created by SL on 14-4-13.
//  Copyright (c) 2014年 cyx. All rights reserved.
//

#import <UIKit/UIKit.h>

//刷新类型
typedef enum
{
    //加载中
    APP_Loading=8534,
    //加载失败，可重新加载
    APP_LoadingCycle,
    //加载完成，没有数据显示，不需要重新加载，友好提示
    APP_LoadingDone,
    //加载完成，不显示加载框
    APP_LoadingRemove,
    //自定义提示图片
    CDELLoadingCustom,
}APPLoadingType;

@interface APPLoadingView : UIView

/**
 *  显示加载框，可重复加载
 *
 *  @param type        刷新类型
 *  @param cycle       可重复加载
 *  @param title       文字说明
 *  @param buttonTitle 重复加载按钮
 */
- (void)showAPPLoadingView:(APPLoadingType)type
                     cycle:(void(^)())cycle
                     title:(NSString *)title
               buttonTitle:(NSString *)buttonTitle
               customImage:(NSString *)customImage;

@end
