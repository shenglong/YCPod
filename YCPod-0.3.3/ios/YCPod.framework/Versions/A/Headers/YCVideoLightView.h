//
//  AppLightView.h
//  Test
//
//  Created by Sheng long on 16/1/28.
//  Copyright © 2016年 Sheng long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCVideoLightView : UIView

+ (void)show;

@end
