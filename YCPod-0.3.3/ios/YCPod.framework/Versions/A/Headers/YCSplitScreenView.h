//
//  YCSplitScreenView.h
//  Test
//
//  Created by Sheng long on 16/2/1.
//  Copyright © 2016年 Sheng long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCSplitScreenView : UIView

@property (nonatomic,strong) UITableView *mainTable;

@property (nonatomic,assign) CGFloat titleWidth;
@property (nonatomic,assign) CGFloat titleHeight;
@property (nonatomic,strong) NSArray *titleArray;

/**
 *  选中位置，默认不选中
 */
@property (nonatomic,assign) NSInteger selectedIndex;

- (void)showView:(UIView *(^)(NSInteger index))selected;

@end
