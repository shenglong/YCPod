//
//  NavigationBarViewController.h
//  PhoneApp
//
//  Created by SL on 15/3/6.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YC_NavigatonBarView.h"

@interface YC_NavigationBarViewController : UIViewController<YC_NavigatonBarViewDelegate>

/**
 *  导航栏
 */
@property (nonatomic,strong) YC_NavigatonBarView *navigationBarView;

/**
 *  当前页面是否禁用ios7返回手势，默认不禁用
 */
@property (nonatomic,assign) BOOL closeInteractiveGesture;

/**
 *  当前页面是否禁用IQKeyboardManager，默认不禁用
 *  不显示显示IQKeyboardManager键盘顶部的自定义view，需要设置inputAccessoryView为[UIView new]
 */
@property (nonatomic,assign) BOOL closeIQKeyboardManager;

/**
 *  关闭键盘顶部菜单栏
 */
@property (nonatomic,assign) BOOL closeIQKeyboardToolbar;

@end
