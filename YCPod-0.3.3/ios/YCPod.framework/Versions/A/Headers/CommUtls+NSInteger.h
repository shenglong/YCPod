//
//  CommUtls+NSString.h
//  Pods
//
//  Created by SL on 16/1/15.
//
//

#import "CommUtls.h"

@interface CommUtls (NSInteger)

/**
 *  将num除以一个数，0舍>0入
 *
 *  @param num    除数
 *  @param divide 被除数
 *
 *  @return
 */
+ (NSInteger)numOverInt:(CGFloat)num
                 divide:(CGFloat)divide;

/**
 *  将浮点型转换成字符串，保留小数，去掉小数点最后的0
 *
 *  @param value 要转换的值
 *  @param num   保留几位小数，最多支持5位
 *
 *  @return
 */
+ (NSString *)numberForString:(CGFloat)value
                          num:(NSInteger)num;

/**
 *  将阿拉伯数字转换为中文数字
 */
+ (NSString *)translationArabicNum:(NSInteger)arabicNum;

@end
