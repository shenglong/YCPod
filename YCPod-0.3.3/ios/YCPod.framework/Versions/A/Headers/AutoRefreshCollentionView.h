//
//  AutoRefreshCollentionView.h
//  Test
//
//  Created by Sheng long on 15/9/9.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import "AutoListView.h"

@interface AutoRefreshCollentionView : AutoListView<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong) UICollectionView *mainTable;

@end
