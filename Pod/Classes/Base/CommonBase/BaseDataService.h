//
//  LoadDataService.h
//  MobileClassPhone
//
//  Created by cyx on 14/12/4.
//  Copyright (c) 2014年 CDEL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#define YC_BuildCacheKey(suffix)   [NSString stringWithFormat:@"%@_CACHE_%@", self.class, suffix];

typedef NS_ENUM (NSInteger, YC_CacheInfo)
{
    NoCache = 0, //没缓存
    ValidCache = 1, //有缓存
    InValidCache = 2 //缓存过期
};

@protocol BaseDataServiceDelegate <NSObject>

- (RACSignal *)loadNetData:(int)type parameters:(id)params,...;

- (RACSignal *)loadLocalData:(int)type parameters:(id)params,...;

- (YC_CacheInfo)getCacheInfo:(int)type parameters:(id)params,...;

@end

@interface BaseDataService : NSObject<BaseDataServiceDelegate>

- (RACSignal *)loadData:(int)type parameters:(id)params,...;

- (RACSignal *)loadNetData:(int)type parameters:(id)params,...;

- (RACSignal *)loadLocalData:(int)type parameters:(id)params,...;

- (YC_CacheInfo)getCacheInfo:(int)type parameters:(id)params,...;

@end
