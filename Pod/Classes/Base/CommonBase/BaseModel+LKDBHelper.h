//
//  YC_BaseModel+LKDBHelper.h
//  Pods
//
//  Created by Sheng long on 16/2/28.
//
//

#import "BaseModel.h"

@interface BaseModel (LKDBHelper)

+ (NSArray *)searchAll;
// !插入Model数组，执行replace
+ (void)insertAll:(NSArray *)models;
+ (void)clearAndInsertAll:(NSArray *)models;
+ (void)clearAndInsertAll:(NSArray *)models where:(NSString *)where;

@end
