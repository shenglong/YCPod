//
//  LoadDataService.m
//  MobileClassPhone
//
//  Created by cyx on 14/12/4.
//  Copyright (c) 2014年 CDEL. All rights reserved.
//

#import "BaseDataService.h"
#import "NetStatusHelper.h"
#import "AppMacro.h"

@implementation BaseDataService

-(void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (RACSignal *)loadData:(int)type parameters:(id)params,...
{
    if ([self getCacheInfo:type parameters:params] == NoCache || [self getCacheInfo:type parameters:params] == InValidCache) {
        if ([NetStatusHelper sharedInstance].netStatus != NoneNet)
            return [self loadNetData:type parameters:params];
        else if([NetStatusHelper sharedInstance].netStatus == NoneNet && [self getCacheInfo:type parameters:params] == InValidCache)
            return [self loadLocalData:type parameters:params];
        else if([NetStatusHelper sharedInstance].netStatus == NoneNet && [self getCacheInfo:type parameters:params] == NoCache)
        {
            NSError *aError = [NSError errorWithDomain:AppCustomErrorDomain code:AppNoNet userInfo:@{AppErrorMsg:AppErrorMsg_NoNet}];
            return [RACSignal error:aError];
        }
        
    } else if ([self getCacheInfo:type parameters:params] == ValidCache) {
        return [self loadLocalData:type parameters:params];
    }
    NSError *aError = [NSError errorWithDomain:AppCustomErrorDomain code:AppNoNet userInfo:@{AppErrorMsg:AppErrorMsg_NoNet}];
    
    return [RACSignal error:aError];
}

- (RACSignal *)loadNetData:(int)type parameters:(id)params,...
{
    return [RACSignal empty];
}

- (RACSignal *)loadLocalData:(int)type parameters:(id)params,...
{
    return [RACSignal empty];
}

- (YC_CacheInfo)getCacheInfo:(int)type parameters:(id)params,...
{
    return NoCache;
}

@end
