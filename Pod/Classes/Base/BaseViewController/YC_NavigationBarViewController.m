//
//  NavigationBarViewController.m
//  PhoneApp
//
//  Created by SL on 15/3/6.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "YC_NavigationBarViewController.h"
#import <Masonry/Masonry.h>
#import <IQKeyboardManager/KeyboardManager.h>

@interface YC_NavigationBarViewController ()

@end

@implementation YC_NavigationBarViewController

-(void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    //添加导航条
//    YC_NavigatonBarView *navigationBarView = [[YC_NavigatonBarView alloc] initLeftButtonPicNormal:nil
//                                                                     leftButtonPicHighlight:nil
//                                                                       rightButtonPicNormal:nil
//                                                                    rightButtonPicHighlight:nil
//                                                                                  fontColor:[UIColor blackColor]];
//    [self.view addSubview:navigationBarView];
//    navigationBarView.navagationBarStyle = Left_button_Show;
//    [navigationBarView setBackgroundColor:[UIColor redColor]];
//    [navigationBarView setDelegate:self];
//    [navigationBarView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.right.mas_equalTo(0);
//        make.height.mas_equalTo(65);
//    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.closeIQKeyboardManager) {
        [[IQKeyboardManager sharedManager] setEnable:NO];
    }
    if (self.closeIQKeyboardToolbar) {
        [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    }
//    //防止从相机页面返回，顶部状态栏颜色不变成白色
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.closeIQKeyboardManager) {
        [[IQKeyboardManager sharedManager] setEnable:YES];
    }
    if (self.closeIQKeyboardToolbar) {
        [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    }
}

-(void)leftButtonClick{
#ifdef DEBUG
    NSLog(@"leftButtonClick");
#endif
    if (self.navigationController.viewControllers.count > 1 && self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)rightButtonClick{
#ifdef DEBUG
    NSLog(@"rightButtonClick");
#endif
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = !self.closeInteractiveGesture;
}

#pragma mark - 转屏
- (BOOL)shouldAutorotate {
    return YES;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
