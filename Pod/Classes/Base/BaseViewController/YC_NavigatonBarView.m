//
//  NavigatonBarView.m
//  forum
//
//  Created by cyx on 12-7-26.
//  Copyright (c) 2012年 cdeledu. All rights reserved.
//

#import "YC_NavigatonBarView.h"
#import <Masonry/Masonry.h>

#define NAVIGATIONBAR_BUTTON_TOP_GAP                20
#define NAVIGATIONBAR_BUTTON_WIDTH                  60
#define NAVIGATIONBAR_TITLE_FONT                    20.0
#define NAVIGATIONBAR_FONT                          15.0

@interface YC_NavigatonBarView()
{
    UIImage *leftButtonImageNormarl;
    UIImage *leftButtonImageHighlight;
    UIImage *rightButtonImageNormarl;
    UIImage *rightButtonImageHighlight;
}
@end

@implementation YC_NavigatonBarView

-(void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (id)initLeftButtonPicNormal:(UIImage *)leftImageNormal
       leftButtonPicHighlight:(UIImage *)leftImageHighlight
         rightButtonPicNormal:(UIImage *)rightImageNormal
      rightButtonPicHighlight:(UIImage *)rightImageHighlight
                    fontColor:(UIColor *)color
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        leftButtonImageNormarl = leftImageNormal;
        leftButtonImageHighlight = leftImageHighlight;
        rightButtonImageNormarl = rightImageNormal;
        rightButtonImageHighlight = rightImageHighlight;
        
        _backGroundImgeView = [UIImageView new];
        [_backGroundImgeView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment= NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:NAVIGATIONBAR_TITLE_FONT];
        _titleLabel.textColor = color;
        [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.backgroundColor = [UIColor clearColor];
        [_leftButton addTarget:self action:@selector(navigationLeftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_leftButton setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        _rightButton = [UIButton  buttonWithType:UIButtonTypeCustom];
        _rightButton.backgroundColor=[UIColor clearColor];
        [_rightButton addTarget:self action:@selector(navigationRightButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_rightButton setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        _leftLabel = [UILabel new];
        _leftLabel.backgroundColor = [UIColor clearColor];
        _leftLabel.textAlignment = NSTextAlignmentCenter;
        _leftLabel.font = [UIFont systemFontOfSize:NAVIGATIONBAR_FONT];
        _leftLabel.textColor = color;
        [_leftLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        _rightLabel = [UILabel new];
        _rightLabel.backgroundColor = [UIColor clearColor];
        _rightLabel.textAlignment = NSTextAlignmentCenter;
        _rightLabel.font = [UIFont systemFontOfSize:NAVIGATIONBAR_FONT];
        _rightLabel.textColor = color;
        [_rightLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self addSubview:_backGroundImgeView];
        [self addSubview:_titleLabel];
        [self addSubview:_leftButton];
        [self addSubview:_rightButton];
        [self addSubview:_rightLabel];
        [self addSubview:_leftLabel];

        [_backGroundImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
        
        [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(NAVIGATIONBAR_BUTTON_TOP_GAP);
            make.left.bottom.mas_equalTo(0);
            make.width.mas_equalTo(NAVIGATIONBAR_BUTTON_WIDTH);
        }];
        
        [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(_leftButton).mas_equalTo(0);
        }];
        
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(NAVIGATIONBAR_BUTTON_TOP_GAP);
            make.right.bottom.mas_equalTo(0);
            make.width.mas_equalTo(NAVIGATIONBAR_BUTTON_WIDTH);
        }];
        
        [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(_rightButton).mas_equalTo(0);
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(NAVIGATIONBAR_BUTTON_TOP_GAP);
            make.left.mas_equalTo(_leftButton.mas_right);
            make.right.mas_equalTo(_rightButton.mas_left);
            make.bottom.mas_equalTo(0);
        }];
    }
    return self;
}

- (void)navigationLeftButtonClick
{
    if([_delegate respondsToSelector:@selector(leftButtonClick)])
        [_delegate leftButtonClick];
}

- (void)navigationRightButtonClick
{
    if([_delegate respondsToSelector:@selector(rightButtonClick)])
        [_delegate rightButtonClick];
}

- (void)setNavagationBarStyle:(YC_NavigationBarStyle)navagationBarStyle
{
    [_leftButton setHidden:YES];
    [_leftLabel setHidden:YES];
    [_rightButton setHidden:YES];
    [_rightLabel setHidden:YES];
    
    [_leftButton setImage:leftButtonImageNormarl forState:UIControlStateNormal];
    [_leftButton setImage:leftButtonImageHighlight forState:UIControlStateHighlighted];
    [_rightButton setImage:rightButtonImageNormarl forState:UIControlStateNormal];
    [_rightButton setImage:rightButtonImageHighlight forState:UIControlStateHighlighted];
    
    switch (navagationBarStyle) {
        case Left_right_button_show:
        {
            [_leftButton setHidden:NO];
            [_leftLabel setHidden:NO];
            [_rightButton setHidden:NO];
            [_rightLabel setHidden:NO];
        }
            break;
        case Left_button_Show:
        {
            [_leftButton setHidden:NO];
            [_leftLabel setHidden:NO];
        }
            break;
        case Right_button_show:
        {
            [_rightButton setHidden:NO];
            [_rightLabel setHidden:NO];
        }
            break;
        default:
            break;
    }
}

@end
