//
//  VPPlayerModel.h
//  Pods
//
//  Created by SL on 16/8/12.
//
//

#import <Foundation/Foundation.h>

@interface VPPlayerModel : NSObject

/**
 *  标题
 */
@property (nonatomic,strong) NSString *title;

/**
 *  播放地址
 */
@property (nonatomic,strong) NSString *playUrl;

/**
 *  播放到的位置
 */
@property (nonatomic,assign) NSInteger currentTime;

/**
 *  是否播放本地地址
 */
@property (nonatomic,assign) BOOL isLocal;

#pragma mark - 播放中
/**
 *  总时长
 */
@property (nonatomic,assign) NSInteger totalTime;

/**
 *  当前学习时长
 */
@property (nonatomic,assign) NSInteger studyTime;

@end
