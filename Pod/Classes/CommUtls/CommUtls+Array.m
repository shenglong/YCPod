//
//  CommUtls+Array.m
//  Pods
//
//  Created by SL on 15/12/24.
//
//

#import "CommUtls+Array.h"
#import "CommUtls+Object.h"

@implementation CommUtls (Array)

+ (NSArray *)assemblyArray:(NSArray *)array
                    number:(NSInteger)number{
    NSMutableArray *totalArray = [NSMutableArray new];
    NSMutableArray *nsarray = [NSMutableArray new];
    for (int i = 0 ; i<[array count]; i++) {
        //当正好是number个并且有数据时
        if (i%number==0 && [nsarray count]>0) {
            [totalArray addObject:[NSArray arrayWithArray:nsarray]];
            [nsarray removeAllObjects];
        }
        [nsarray addObject:[array objectAtIndex:i]];
    }
    //最后一组数据
    if ([nsarray count]>0) {
        [totalArray addObject:[NSArray arrayWithArray:nsarray]];
    }
    return totalArray;
}

+ (NSArray *)sequenceArray:(NSArray *)array
                      text:(NSString *)text
                      desc:(BOOL)desc{
    if (array.count == 0) {
        return array;
    }
    
    //排序
    NSMutableArray *marray1 = [NSMutableArray arrayWithArray:array];
    
    for (int i = 0; i < marray1.count-1; i++) {
        for (int j = 0; j < marray1.count-1-i; j++) {
            NSDictionary *dic1 = [marray1 objectAtIndex:j];
            NSDictionary *dic2 = [marray1 objectAtIndex:(j+1)];
            
            BOOL change = NO;
            if (desc && [[dic2 objectForKey:text] intValue] > [[dic1 objectForKey:text] intValue]) {
                change = YES;
            } else if (!desc && [[dic2 objectForKey:text] intValue] < [[dic1 objectForKey:text] intValue]) {
                change = YES;
            }
            if (change) {
                [marray1 replaceObjectAtIndex:j withObject:dic2];
                [marray1 replaceObjectAtIndex:(j+1) withObject:dic1];
            }
        }
    }
    return marray1;
}

+ (NSArray *)packageArray:(NSArray *)fromArray
                     text:(NSString *)text
                 forArray:(NSArray *)toArray
                      key:(NSString *)key{
    if (toArray.count == 0) {
        return toArray;
    }
    
    //fromArray数据组装
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
    for (NSDictionary *d1 in fromArray) {
        NSString *title = [NSString stringWithFormat:@"%@", [d1 objectForKey:text]];
        if (![dic objectForKey:title]) {
            [dic setObject:[NSMutableArray arrayWithCapacity:1] forKey:title];
        }
        NSMutableArray *array = [dic objectForKey:title];
        [array addObject:d1];
    }
    
    //组装数据
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
    for (int i = 0; i < toArray.count; i++) {
        NSDictionary *d2 = [toArray objectAtIndex:i];
        NSString *title = [NSString stringWithFormat:@"%@", [d2 objectForKey:text]];
        
        NSMutableDictionary *d3 = [NSMutableDictionary dictionaryWithDictionary:d2];
        [array addObject:d3];
        if ([dic objectForKey:title]) {
            [d3 setObject:[dic objectForKey:title] forKey:key];
        }
    }
    
    return array;
}

/**
 *  排序
 *
 *  @param data <#data description#>
 *
 *  @return <#return value description#>
 */
+ (NSArray *)sequenceSort:(NSArray *)data text:(NSString *)text desc:(BOOL)desc {
    //排序
    NSMutableArray *marray1 = [NSMutableArray arrayWithArray:data];
    for (int i = 0; i < marray1.count-1; i++) {
        for (int j = 0; j < marray1.count-1-i; j++) {
            id dic1 = [marray1 objectAtIndex:j];
            id dic2 = [marray1 objectAtIndex:(j+1)];
            
            BOOL change = NO;
            if (desc && [[self objectAttributeValue:dic2 attribute:text] integerValue] > [[self objectAttributeValue:dic1 attribute:text] integerValue]) {
                change = YES;
            } else if (!desc && [[self objectAttributeValue:dic2 attribute:text] integerValue] < [[self objectAttributeValue:dic1 attribute:text] integerValue]) {
                change = YES;
            }
            if (change) {
                [marray1 replaceObjectAtIndex:j withObject:dic2];
                [marray1 replaceObjectAtIndex:(j+1) withObject:dic1];
            }
        }
    }
    return marray1;
}

/**
 *  获取对象属性的值
 *
 *  @param object    该对象
 *  @param attribute 属性名
 *
 *  @return 值
 */
+ (id)objectAttributeValue:(id)object attribute:(NSString *)attribute{
    NSDictionary *data = [self objectProperties:object];
    return data[attribute];
}

@end
