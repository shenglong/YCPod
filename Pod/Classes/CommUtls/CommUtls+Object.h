//
//  CommUtls+Object.h
//  Pods
//
//  Created by SL on 16/6/30.
//
//

#import "CommUtls.h"

@interface CommUtls (Object)

/**
 *  获取对象的属性值对应字典
 *
 *  @param object 对象
 *
 *  @return 属性对应字典
 */
+ (NSDictionary *)objectProperties:(id)object;

@end
