//
//  CommUtls+Object.m
//  Pods
//
//  Created by SL on 16/6/30.
//
//

#import "CommUtls+Object.h"
#import <objc/runtime.h>

@implementation CommUtls (Object)

+ (NSDictionary *)objectProperties:(id)object {
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([object class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        id propertyValue = [object valueForKey:(NSString *)propertyName];
        if (propertyValue) [props setObject:propertyValue forKey:propertyName];
    }
    free(properties);
    return props;
}

@end
