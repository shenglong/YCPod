//
//  CommUtls+Time.m
//  CdeleduUtls
//
//  Created by 陈轶翔 on 14-3-30.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

#import "CommUtls+Time.h"

@implementation CommUtls (Time)

/**
 *	@brief	格式化时间为字符串
 *
 *	@param 	date 	NSDate系统时间类型
 *
 *	@return	返回默认格式yyyy-MM-dd HH:mm:ss
 */
+ (NSString *)encodeTime:(NSDate *)date

{
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [formatter stringFromDate:date];
    }
    @catch (NSException *exception) {
        return @"";
    }
    @finally {
    }
}

/**
 *	@brief	字符串格式化为时间格式
 *
 *	@param 	dateString 	默认格式yyyy-MM-dd HH:mm:ss
 *
 *	@return	返回时间格式
 */
+ (NSDate *)dencodeTime:(NSString *)dateString
{
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [formatter dateFromString:dateString];
    }
    @catch (NSException *exception)
    {

    }
    @finally {
    }
}

+ (NSString *)convertDateFromCST:(NSString *)_date
{
    if (_date == nil) {
        return nil;
    }
    //return nil;
    NSLog(@"_date==%@",_date);
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"EEE MMM dd HH:mm:ss 'CST' yyyy"];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] ];
    
    
    NSDate *formatterDate = [inputFormatter dateFromString:_date];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *newDateString = [outputFormatter stringFromDate:formatterDate];
    return newDateString;
}

/**
 *	@brief	离现在时间相差时间
 *
 *	@param 	date 	时间格式
 *
 *	@return	返回字符串
 */
+ (NSString *)timeSinceNow:(NSDate *)date

{
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        NSTimeInterval interval = 0 - [date timeIntervalSinceNow];
        
        if (interval < 60) {
            // 几秒前
            return @"1分钟内";
        } else if (interval < (60 * 60)) {
            // 几分钟前
            return [NSString stringWithFormat:@"%u分钟前", (int)(interval / 60)];
        } else if (interval < (24 * 60 * 60)) {
            // 几小时前
            return [NSString stringWithFormat:@"%u小时前", (int)(interval / 60 / 60)];
        } else if (interval < (2 * 24 * 60 * 60)) {
            // 昨天
            [formatter setDateFormat:@"昨天"];
            return [formatter stringFromDate:date];
        } else if (interval < (3 * 24 * 60 * 60)) {
            // 前天
            [formatter setDateFormat:@"前天"];
            return [formatter stringFromDate:date];
        }  else if (interval < (30 * 24 * 60 * 60)) {
            // 具体时间
            NSInteger days = interval / (24 * 60 * 60);
            return [NSString stringWithFormat:@"%ld天前",(long)days];
        }  else if (interval < (365 * 24 * 60 * 60)) {
            // 具体时间
            NSInteger num = interval / (30 * 24 * 60 * 60);
            return [NSString stringWithFormat:@"%ld个月前",(long)num];
        } else {
            // 具体时间
            NSInteger num = interval / (365 * 24 * 60 * 60);
            return [NSString stringWithFormat:@"%ld年前",(long)num];
        }
    }
    @catch (NSException *exception) {
        return @"";
    }
    @finally {
    }
}

/**
 *	@brief	把秒转化为时间字符串显示，播放器常用
 *
 *	@param 	durartion 	传入参数
 *
 *	@return	播放器播放进度时间，比如
 */
+ (NSString *)changeSecondsToString:(CGFloat)durartion
{
    int hh = durartion/(60 * 60);
    int mm = hh > 0 ? (durartion - 60*60)/60 : durartion/60;
    int ss = (int)durartion%60;
    NSString *hhStr,*mmStr,*ssStr;
    if (hh == 0) {
        hhStr = @"00";
    }else if (hh > 0 && hh < 10) {
        hhStr = [NSString stringWithFormat:@"0%d",hh];
    }else {
        hhStr = [NSString stringWithFormat:@"%d",hh];
    }
    if (mm == 0) {
        mmStr = @"00";
    }else if (mm > 0 && mm < 10) {
        mmStr = [NSString stringWithFormat:@"0%d",mm];
    }else {
        mmStr = [NSString stringWithFormat:@"%d",mm];
    }
    if (ss == 0) {
        ssStr = @"00";
    }else if (ss > 0 && ss < 10) {
        ssStr = [NSString stringWithFormat:@"0%d",ss];
    }else {
        ssStr = [NSString stringWithFormat:@"%d",ss];
    }
    return [NSString stringWithFormat:@"%@:%@:%@",hhStr,mmStr,ssStr];
}

+ (NSString *)timeToHMS:(CGFloat)time{
    NSInteger hh = time/3600;
    NSInteger mm = (time-3600*hh)/60;
    NSInteger ss = (time-3600*hh-60*mm);
    
    NSString *hhStr = [NSString stringWithFormat:@"%@%d",hh<10?@"0":@"",hh];
    NSString *mmStr = [NSString stringWithFormat:@"%@%d",mm<10?@"0":@"",mm];
    NSString *ssStr = [NSString stringWithFormat:@"%@%d",ss<10?@"0":@"",ss];
    
    return [NSString stringWithFormat:@"%@:%@:%@",hhStr,mmStr,ssStr];
}

+ (NSString *)timeToMS:(CGFloat)time {
    NSInteger mm = time/60;
    NSInteger ss = (time-60*mm);
    
    NSString *mmStr = [NSString stringWithFormat:@"%@%d",mm<10?@"0":@"",mm];
    NSString *ssStr = [NSString stringWithFormat:@"%@%d",ss<10?@"0":@"",ss];
    
    return [NSString stringWithFormat:@"%@:%@",mmStr,ssStr];
}

+ (NSInteger)hmsToTime:(NSString *)text{
    NSInteger total = 0;
    NSArray *length = [text componentsSeparatedByString:@":"];
    for (NSInteger i = length.count; i>0; i--) {
        NSInteger c = [length[i-1] integerValue];
        total+=c*pow(60, length.count-i);
    }
    return total;
}

/**
 *	@brief	格式化时间为字符串
 *
 *	@param 	date 	时间
 *	@param 	format 	格式化字符串
 *
 *	@return	返回时间字符串
 */
+ (NSString *)encodeTime:(NSDate *)date format:(NSString *)format

{
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:format];
        return [formatter stringFromDate:date];
    }
    @catch (NSException *exception) {
        return @"";
    }
    @finally {
    }
    
}

/**
 *	@brief  格式化成时间格式
 *
 *	@param 	dateString 	时间字符串
 *	@param 	format 	格式化字符串
 *
 *	@return	返回时间格式
 */
+ (NSDate *)dencodeTime:(NSString *)dateString format:(NSString *)format

{
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:format];
        return [formatter dateFromString:dateString];
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}


#pragma mark - 天天酷玩
//今天是否属于周末
+(BOOL)todayIsWeekend
{
    NSInteger type = [self getWeekdayFromDate:[NSDate date]];
    if (type == 1 || type == 7) {
        return YES;
    }
    return NO;
}

//返回当前日期后total天的工作日期
+(NSArray*)getWorkNumber:(NSInteger)total
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
    
    NSMutableArray *array1 = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *array2 = [NSMutableArray arrayWithCapacity:1];
    [array addObject:array1];
    [array addObject:array2];
    
    NSDate *date = [NSDate date];
    for (int i = 0; i < CGFLOAT_MAX; i++) {
        NSDate *newDate = [self previousDay:date num:(i+1)];
        NSInteger type = [self getWeekdayFromDate:newDate];
        if (!(type == 1 || type == 7)) {
            NSArray *array3 = [self getDateString:newDate];
            [array1 addObject:[array3 objectAtIndex:0]];
            [array2 addObject:[array3 objectAtIndex:1]];
        }
        if (array1.count==total) {
            break;
        }
    }
    return array;
}

//返回当前日期后total天的周末日期
+(NSArray*)getWeekendNumber:(NSInteger)total
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
    
    NSMutableArray *array1 = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *array2 = [NSMutableArray arrayWithCapacity:1];
    [array addObject:array1];
    [array addObject:array2];
    
    NSDate *date = [NSDate date];
    for (int i = 0; i < CGFLOAT_MAX; i++) {
        NSDate *newDate = [self previousDay:date num:(i+1)];
        NSInteger type = [self getWeekdayFromDate:newDate];
        if (type == 1 || type == 7) {
            NSArray *array3 = [self getDateString:newDate];
            [array1 addObject:[array3 objectAtIndex:0]];
            [array2 addObject:[array3 objectAtIndex:1]];
        }
        if (array1.count==total) {
            break;
        }
    }
    return array;
}

+(NSArray*)getDateString:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *time1 = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"MM月dd日"];
    NSString *time2 = [formatter stringFromDate:date];
    
    return @[time1,time2];
}

//返回date日期的num天
+ (NSDate *)previousDay:(NSDate *)date num:(NSInteger)num
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:num];
    return [calendar dateByAddingComponents:comps toDate:date options:0];
}

//返回date日期月份内的第一天
+ (NSDate *)firstDayOfMonthContainingDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    [comps setDay:1];
    return [calendar dateFromComponents:comps];
}

//返回date日期的上一天
+ (NSDate *)previousDay:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:-1];
    return [calendar dateByAddingComponents:comps toDate:date options:0];
}

//判断date日期与comparetime日期是否在一个月内
+ (BOOL)dateIsInMonthShowing:(NSDate *)date comparetime:(NSDate *)comparetime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps1 = [calendar components:(NSMonthCalendarUnit) fromDate:comparetime];
    NSDateComponents *comps2 = [calendar components:(NSMonthCalendarUnit) fromDate:date];
    return comps1.month == comps2.month;
}

//返回星期几     从周日开始，周日为1
+ (NSUInteger)getWeekdayFromDate:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    NSCalendar* calendar = [formatter calendar];
    
    //    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    components = [calendar components:unitFlags fromDate:date];
    NSUInteger weekday = [components weekday];
    
    //
    return weekday;
}

//聊天显示时间
+(NSString *)getShowTime:(NSString *)time
{
    @try {
        
        NSString *mode = @"yyyy-MM-dd";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSDate *date = [self dencodeTime:time];
        
        NSString *yy1 = [self encodeTime:[self dencodeTime:time] format:mode];
        
        NSString *yy0 = [self encodeTime:[NSDate date] format:mode];
        NSString *yy2 = [self encodeTime:[self previousDay:[NSDate date] num:-1] format:mode];
        NSString *yy3 = [self encodeTime:[self previousDay:[NSDate date] num:-2] format:mode];
        
        if ([yy1 isEqualToString:yy0]) {
            // 几秒前
            [formatter setDateFormat:@"HH:mm"];
            return [formatter stringFromDate:date];
        } else if ([yy1 isEqualToString:yy2]) {
            // 昨天
            [formatter setDateFormat:@"昨天 HH:mm"];
            return [formatter stringFromDate:date];
        } else if ([yy1 isEqualToString:yy3]) {
            // 前天
            [formatter setDateFormat:@"前天 HH:mm"];
            return [formatter stringFromDate:date];
        } else {
            // 具体时间
            [formatter setDateFormat:@"MM-dd HH:mm"];
            return [formatter stringFromDate:date];
        }
    }
    @catch (NSException *exception) {
        return @"";
    }
    @finally {
    }
    
    return nil;
}

@end
