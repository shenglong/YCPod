//
//  CommUtls+NSString.m
//  Pods
//
//  Created by SL on 16/1/15.
//
//

#import "CommUtls+NSString.h"

@implementation CommUtls (NSString)

+ (BOOL)verificationNumber:(NSString *)text
                       min:(NSInteger)min
                       max:(NSInteger)max{
    if (text != nil && ![text isEqual:[NSNull null]]) {
        NSString *str = [text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (str.length>=min && str.length<=max) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)getPhoneFormat:(NSString*)phone{
    if (phone != nil && ![phone isEqual:[NSNull null]]) {
        NSString *regexx = @"^((13[0-9])|(17[0,0-9])|(147)|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
        NSPredicate *predd = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexx];
        return [predd evaluateWithObject:phone];
    }
    return NO;
}

+ (NSString *)deleteSpace:(NSString *)text{
    return [text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSAttributedString *)fetchCustomString:(NSArray *)titles
                                    fonts:(NSArray *)fonts
                                   colors:(NSArray *)colors{
    
    NSString *text = [titles componentsJoinedByString:@""];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text];
    __block NSInteger start = 0;
    [titles enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [string addAttributes:@{
                                NSFontAttributeName:fonts[idx],
                                NSForegroundColorAttributeName:colors[idx],
                                }
                        range:NSMakeRange(start, obj.length)];
        start+=obj.length;
//        start+=(obj.length+1);
    }];
    return string;
}

@end
