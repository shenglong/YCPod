//
//  CommUtls+Time.h
//  CdeleduUtls
//
//  Created by 陈轶翔 on 14-3-30.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

#import "CommUtls.h"

@interface CommUtls (Time)

/*时间处理*/
//CST时间格式转换
+ (NSString *)convertDateFromCST:(NSString *)_date;

//转换NSDate格式为字符串"yyyy-MM-dd HH:mm:ss"
+ (NSString *)encodeTime:(NSDate *)date;

//转换字符串为"yyyy-MM-dd HH:mm:ss"格式到NSDate
+ (NSDate *)dencodeTime:(NSString *)dateString;

//装换NSDate格式到NString
+ (NSString *)encodeTime:(NSDate *)date format:(NSString *)format;

//转换NString到NSdate
+ (NSDate *)dencodeTime:(NSString *)dateString format:(NSString *)format;

//从现在到某天的时间
+ (NSString *)timeSinceNow:(NSDate *)date;

//把秒转化为时间字符串显示，播放器常用
+ (NSString *)changeSecondsToString:(CGFloat)durartion;

/**
 *  将秒数转换成00:00:00
 */
+ (NSString *)timeToHMS:(CGFloat)time;
+ (NSString *)timeToMS:(CGFloat)time;

/**
 *  将00:00:00的数据转换成秒数
 */
+ (NSInteger)hmsToTime:(NSString *)text;

#pragma mark -
//今天是否属于周末
+(BOOL)todayIsWeekend;

//返回当前日期后total天的工作日期
+(NSArray*)getWorkNumber:(NSInteger)total;

//返回当前日期后total天的周末日期
+(NSArray*)getWeekendNumber:(NSInteger)total;

//返回date日期的num天
+ (NSDate *)previousDay:(NSDate *)date num:(NSInteger)num;

//聊天显示时间
+(NSString *)getShowTime:(NSString *)time;

@end
