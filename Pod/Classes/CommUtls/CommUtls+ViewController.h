//
//  CommUtls+ViewController.h
//  Pods
//
//  Created by SL on 15/12/24.
//
//

#import "CommUtls.h"

@interface CommUtls (ViewController)

/**
 *  获取当前屏幕显示的viewcontroller
 */
+ (UIViewController *)getCurrentVC;

/**
 *  获取持有当前UIView的UIViewController
 *
 *  @param sourceView
 *
 *  @return
 */
+ (UIViewController *)findViewController:(UIView *)sourceView;

@end
