//
//  CommUtls+DeviceInfo.h
//  DL
//
//  Created by cyx on 14-9-19.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

/*
 framework  CoreTelephony  SystemConfiguration
 */

#import "CommUtls.h"

@interface CommUtls (DeviceInfo)

/**
 *  获取设备系统版本
 *
 *  @return 设备系统版本信息
 */
+ (NSString *)getSystemVersion;

/**
 *  获取设备唯一编码
 *
 *  @return 设备唯一编码
 */
+ (NSString *)getUniqueIdentifier;

/**
 *  获取设备运营商信息
 *
 *  @return 运营商名字
 */
+ (NSString *)getOperator;

/**
 *  获取设备屏幕分辨率
 *
 *  @return 屏幕分辨率
 */
+ (NSString *)getResolution;

/**
 *  获取设备型号
 *
 *  @return 设备型号
 */
+ (NSString *)getModel;

/**
 *  获取设备网络状态
 *
 *  @return 设备网络状态
 */
+ (NSString *)getNetworkType;

/**
 *	获取一个唯一码
 */
+ (NSString *)createUuidString;

@end
