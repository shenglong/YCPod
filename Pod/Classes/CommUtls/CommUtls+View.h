//
//  CommUtls+View.h
//  Pods
//
//  Created by Sheng long on 15/12/6.
//
//

#import "CommUtls.h"

@interface CommUtls (View)

/**
 *  将findView下属于belongView的第一个UIScrollView顶部返回设置生效
 *
 *  @param findView
 *  @param belongView 
 */
+ (void)findView:(UIView *)findView belongView:(UIView *)belongView;

/**
 *  UIView转化成UIImage
 *
 *  @param view       转换的view
 *  @param scrollFull view是UIScrollView类型，是否截取contentSize全部内容
 *
 *  @return
 */
+ (UIImage *)createImageFromView:(UIView *)view
                      scrollFull:(BOOL)scrollFull;

/*
 *  压缩图片至目标尺寸
 *
 *  @param sourceImage 源图片
 *  @param targetWidth 图片最终尺寸的宽
 *
 *  @return 返回按照源图片的宽、高比例压缩至目标宽、高的图片
 */
+ (UIImage *)compressImage:(UIImage *)sourceImage toTargetWidth:(CGFloat)targetWidth;

/**
 *  为view设置指定圆角
 *
 *  @param view    <#view description#>
 *  @param bounds  <#bounds description#>
 *  @param corners UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight
 *  @param size    <#size description#>
 */
+ (void)bezierPathWithRoundedRect:(UIView *)view
                           bounds:(CGRect)bounds
                          corners:(UIRectCorner)corners
                             size:(CGSize)size;

@end
