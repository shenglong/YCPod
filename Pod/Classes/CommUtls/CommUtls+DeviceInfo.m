//
//  CommUtls+DeviceInfo.m
//  DL
//
//  Created by cyx on 14-9-19.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

#import "CommUtls+DeviceInfo.h"

#define HEAD   @":"
#define END    @"Mobile Country Code"
#import <CommonCrypto/CommonDigest.h>
#import  <dlfcn.h>
#include <netdb.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <net/if_dl.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#import  <CommonCrypto/CommonCryptor.h>
#import  <SystemConfiguration/SystemConfiguration.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <OpenUDID/OpenUDID.h>
#import <Reachability/Reachability.h>

@implementation CommUtls (DeviceInfo)

+ (NSString *)getSystemVersion
{
  NSString *system = nil;
  system = [[UIDevice currentDevice] systemVersion];
  if(system != nil)
  {
    if([system length]>3)
    {
      system = [system substringToIndex:3];
    }
  }
  return system;
}

+ (NSString *)getUniqueIdentifier
{
  return [OpenUDID value];
}

+ (NSString *)getOperator
{
  CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
  CTCarrier *carrier = info.subscriberCellularProvider;
  NSString * str = [carrier description];
  str = [str substringToIndex:[str rangeOfString:END].location];
  str = [str substringFromIndex:[str rangeOfString:HEAD].location+1];
  str = [str substringFromIndex:[str rangeOfString:@"["].location+1];
  str = [str substringToIndex:[str rangeOfString:@"]"].location];
  return str;
}

+ (NSString *)getResolution
{
  CGRect rect = [[UIScreen mainScreen] bounds];
  CGSize size = rect.size;
  NSString *resolution = [NSString stringWithFormat:@"%f*%f",size.width,size.height];
  return resolution;
}

+ (NSString *)getModel
{
  size_t size;
  sysctlbyname("hw.machine", NULL, &size, NULL, 0);
  char *machine = (char*)malloc(size);
  sysctlbyname("hw.machine", machine, &size, NULL, 0);
  NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
  free(machine);
  return platform;
}

+ (NSString *)getNetworkType
{
  @try {
    Reachability *reach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    NetworkStatus status = [reach currentReachabilityStatus];
    if( status == ReachableViaWiFi)
    {
      return @"wifi";
    }
    else if( status == ReachableViaWWAN)
    {
      return @"wwan";
    }
  }
  @catch (NSException *exception) {
    
  }
  @finally {
  }
  return @"";
}

+ (NSString *)createUuidString {
    CFUUIDRef uuid_ref = CFUUIDCreate(NULL);
    CFStringRef uuid_string_ref = CFUUIDCreateString(NULL, uuid_ref);
    CFRelease(uuid_ref);
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)uuid_string_ref];
    CFRelease(uuid_string_ref);
    return uuid;
}

@end
