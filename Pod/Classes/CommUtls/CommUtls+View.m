//
//  CommUtls+View.m
//  Pods
//
//  Created by Sheng long on 15/12/6.
//
//

#import "CommUtls+View.h"

@implementation CommUtls (View)

+ (void)findView:(UIView *)findView belongView:(UIView *)belongView{
    __block BOOL topResponse = NO;
    [[findView subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView *view1 = (UIView*)obj;
            
            BOOL top = NO;
            if ([view1 isKindOfClass:[UIScrollView class]]) {
                if ([view1 isKindOfClass:[UITableView class]] && !topResponse) {
                    top = [view1 isDescendantOfView:belongView];
                    if (top) {
                        //已经有响应的UIScrollView
                        topResponse = YES;
                    }
                }
                [((UIScrollView*)view1) setScrollsToTop:top];
            }
            if (view1.subviews){
                [self findView:view1 belongView:top?nil:belongView];
            }
        }
    }];
}

+ (UIImage *)createImageFromView:(UIView *)view
                      scrollFull:(BOOL)scrollFull{
    UIImage* image = nil;
    //开始绘图，下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(view.frame.size,NO,0);
    
    if ([view isKindOfClass:[UIScrollView class]] && scrollFull) {
        UIScrollView *scrollView = (UIScrollView *)view;
        CGPoint savedContentOffset = scrollView.contentOffset;
        CGRect savedFrame = scrollView.frame;
        scrollView.contentOffset = CGPointZero;
        scrollView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        
        [scrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        scrollView.contentOffset = savedContentOffset;
        scrollView.frame = savedFrame;
    }else{
        //将view上的子view加进来
        [view.layer renderInContext:UIGraphicsGetCurrentContext()];
        //开始生成图片
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)compressImage:(UIImage *)sourceImage toTargetWidth:(CGFloat)targetWidth {
    CGSize imageSize = sourceImage.size;
    
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    if (width < targetWidth) {
        return sourceImage;
    }
    
    CGFloat targetHeight = (targetWidth / width) * height;
    
    UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, targetWidth, targetHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (void)bezierPathWithRoundedRect:(UIView *)view
                           bounds:(CGRect)bounds
                          corners:(UIRectCorner)corners
                             size:(CGSize)size{
    view.layer.masksToBounds = YES;
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:corners cornerRadii:size];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = bounds;
    maskLayer1.path = maskPath1.CGPath;
    view.layer.mask = maskLayer1;
}

@end
