//
//  CommUtls+OpenSystem.m
//  CdeleduUtls
//
//  Created by 陈轶翔 on 14-3-30.
//  Copyright (c) 2014年 Cdeledu. All rights reserved.
//

#import "CommUtls+OpenSystem.h"

@implementation CommUtls (OpenSystem)

+ (void)goToSmsPage:(NSString *)phoneNumber {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms://%@",phoneNumber]]];
}

+ (void)openBrowse:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openEmail:(NSString *)email {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",email]]];
}

+ (void)openPhone:(NSString*)openPhone inView:(UIView*)inView {
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",openPhone]];
    UIWebView *phoneCallWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
    [phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
    [inView addSubview:phoneCallWebView];
}

+ (void)goToAppStoreHomePage:(NSInteger)appid {
    NSString *url = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/id%d",appid];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)goToAppStoreCommentPage:(NSInteger)appid {
    NSString *url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%ld&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appid];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

@end
