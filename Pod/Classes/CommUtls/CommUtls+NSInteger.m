//
//  CommUtls+NSString.m
//  Pods
//
//  Created by SL on 16/1/15.
//
//

#import "CommUtls+NSInteger.h"

@implementation CommUtls (NSInteger)

+ (NSInteger)numOverInt:(CGFloat)num
                 divide:(CGFloat)divide{
    if (divide!=0) {
        CGFloat v1 = (CGFloat)(num*1./divide);
        NSInteger v2 = (NSInteger)(num/divide);
        if (v1>v2) {
            return v2+1;
        }
        return v2;
    }
    return num;
}

+ (NSString *)numberForString:(CGFloat)value
                          num:(NSInteger)num {
    NSString *txt = nil;
    switch (num) {
        case 0: {
            txt = [NSString stringWithFormat:@"%.0f",value];
        }
            break;
        case 1: {
            txt = [NSString stringWithFormat:@"%.1f",value];
        }
            break;
        case 2: {
            txt = [NSString stringWithFormat:@"%.2f",value];
        }
            break;
        case 3: {
            txt = [NSString stringWithFormat:@"%.3f",value];
        }
            break;
        case 4: {
            txt = [NSString stringWithFormat:@"%.4f",value];
        }
            break;
        case 5: {
            txt = [NSString stringWithFormat:@"%.5f",value];
        }
            break;
        default:
            return [NSString stringWithFormat:@"%f",value];
            break;
    }
    
    NSArray *array = [txt componentsSeparatedByString:@"."];
    if (array.count==2) {
        
        NSString *last = array.lastObject;
        NSInteger loc = 0;
        for (NSInteger i = last.length-1; i>=0; i--) {
            NSString *string = [last substringWithRange:NSMakeRange(i, 1)];
            if ([string integerValue]==0) {
                loc = i-1;
            }else{
                loc = i+1;
                break;
            }
        }
        if (loc<0) {
            return array.firstObject;
        }else{
            return [NSString stringWithFormat:@"%@.%@",array.firstObject,[last substringToIndex:loc]];
        }
    }
    
    return txt;
}

+ (NSString *)translationArabicNum:(NSInteger)arabicNum{
    NSString *arabicNumStr = [NSString stringWithFormat:@"%ld",(long)arabicNum];
    NSArray *arabicNumeralsArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0"];
    NSArray *chineseNumeralsArray = @[@"一",@"二",@"三",@"四",@"五",@"六",@"七",@"八",@"九",@"零"];
    NSArray *digits = @[@"个",@"十",@"百",@"千",@"万",@"十",@"百",@"千",@"亿",@"十",@"百",@"千",@"兆"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:chineseNumeralsArray forKeys:arabicNumeralsArray];
    
    if (arabicNum < 20 && arabicNum > 9) {
        if (arabicNum == 10) {
            return @"十";
        }else{
            NSString *subStr1 = [arabicNumStr substringWithRange:NSMakeRange(1, 1)];
            NSString *a1 = [dictionary objectForKey:subStr1];
            NSString *chinese1 = [NSString stringWithFormat:@"十%@",a1];
            return chinese1;
        }
    }else{
        NSMutableArray *sums = [NSMutableArray array];
        for (int i = 0; i < arabicNumStr.length; i ++)
        {
            NSString *substr = [arabicNumStr substringWithRange:NSMakeRange(i, 1)];
            NSString *a = [dictionary objectForKey:substr];
            NSString *b = digits[arabicNumStr.length -i-1];
            NSString *sum = [a stringByAppendingString:b];
            if ([a isEqualToString:chineseNumeralsArray[9]])
            {
                if([b isEqualToString:digits[4]] || [b isEqualToString:digits[8]])
                {
                    sum = b;
                    if ([[sums lastObject] isEqualToString:chineseNumeralsArray[9]])
                    {
                        [sums removeLastObject];
                    }
                }else
                {
                    sum = chineseNumeralsArray[9];
                }
                
                if ([[sums lastObject] isEqualToString:sum])
                {
                    continue;
                }
            }
            
            [sums addObject:sum];
        }
        NSString *sumStr = [sums  componentsJoinedByString:@""];
        NSString *chinese = [sumStr substringToIndex:sumStr.length-1];
        return chinese;
    }
}

@end
