//
//  UIView+Loading.m
//  MobileClassPhone
//
//  Created by SL on 14/12/26.
//  Copyright (c) 2014年 APP. All rights reserved.
//

#import "UIView+Loading.h"
#import "APPLoadingView.h"

@implementation UIView (Loading)

- (void)LoadingInSelf{
    [[self GetLoadingView] showAPPLoadingView:APP_Loading
                                        cycle:nil
                                        title:nil
                                  buttonTitle:nil
                                  customImage:nil];
}

- (void)LoadingHideInSelf{
    [[self GetLoadingView] showAPPLoadingView:APP_LoadingRemove
                                        cycle:nil
                                        title:nil
                                  buttonTitle:nil
                                  customImage:nil];
}

- (void)LoadingDoneInSelf:(APPLoadingType)type
                    title:(NSString*)title{
    [[self GetLoadingView] showAPPLoadingView:type
                                        cycle:nil
                                        title:title
                                  buttonTitle:nil
                                  customImage:nil];
}

- (void)LoadingCycleInSelf:(void(^)())cycle
                     title:(NSString*)title
               buttonTitle:(NSString*)buttonTitle{
    [[self GetLoadingView] showAPPLoadingView:APP_LoadingCycle
                                        cycle:cycle
                                        title:title
                                  buttonTitle:buttonTitle
                                  customImage:nil];
}

- (void)LoadingCustomInSelf:(NSString *)imageName
                      title:(NSString *)title
                      cycle:(void(^)())cycle
                buttonTitle:(NSString *)buttonTitle{
    [[self GetLoadingView] showAPPLoadingView:CDELLoadingCustom
                                        cycle:cycle
                                        title:title
                                  buttonTitle:buttonTitle
                                  customImage:imageName];
}

//获取加载框
- (APPLoadingView *)GetLoadingView{
    __block APPLoadingView *_loadingView = nil;
    [[self subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[APPLoadingView class]]) {
            if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 10.0) {
                [obj removeFromSuperview];
            } else {
                _loadingView = (APPLoadingView *)obj;
            }
            *stop = YES;
        }
    }];
    if (!_loadingView) {
        _loadingView = [APPLoadingView new];
        [_loadingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_loadingView];
    }
    [self bringSubviewToFront:_loadingView];
    
    return _loadingView;
}

@end
