//
//  YCViewConstant.m
//  Pods
//
//  Created by SL on 15/12/8.
//
//

#import "YCViewConstant.h"
#import "CommUtls.h"

@implementation YCViewConstant

+ (instancetype)sharedInstance {
    static YCViewConstant *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[YCViewConstant alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.loadingColor = [CommUtls colorWithHexString:@"#0099ff"];
    }
    return self;
}

@end
