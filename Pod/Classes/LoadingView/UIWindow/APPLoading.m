//
//  APPLoading.m
//  MobileClassPhone
//
//  Created by SL on 14/12/29.
//  Copyright (c) 2014年 APP. All rights reserved.
//

#import "APPLoading.h"
#import "APPLoadingWindow.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#define ALERT_SHOW_AFTER            .55

@implementation APPLoading{
    APPLoadingWindow *_loadingView;
}

+ (instancetype)sharedManager
{
    static id _sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

+ (void)LoadingInWindow:(NSString *)title
                  close:(void(^)())close{
    APPLoadingWindow *loadingView = [[self sharedManager] getDLLoadingView];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [loadingView showLoading:title close:close inView:window];
}

+ (void)LoadingToolTipInWindow:(NSString *)title{
    APPLoadingWindow *loadingView = [[self sharedManager] getDLLoadingView];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [loadingView showToolTip:title interval:1.5 inView:window];
}

+ (void)LoadingHideInWindow{
    [[[self sharedManager] getDLLoadingView] removeFromSuperview];
}

+ (void)LoadingInWindowAlertAfter:(NSString *)title
                            close:(void(^)())close
                             show:(void(^)())show{
    @weakify(self);
    [[[RACSignal interval:ALERT_SHOW_AFTER
              onScheduler:[RACScheduler currentScheduler]] take:1] subscribeNext:^(id x) {
        @strongify(self);
        [self LoadingInWindow:title close:close];
        show();
    }];
}

+ (void)LoadingToolTipInWindowAlertAfter:(NSString *)title{
    @weakify(self);
    [[[RACSignal interval:ALERT_SHOW_AFTER
              onScheduler:[RACScheduler currentScheduler]] take:1] subscribeNext:^(id x) {
        @strongify(self);
        [self LoadingToolTipInWindow:title];
    }];
}

- (APPLoadingWindow *)getDLLoadingView{
    if (!_loadingView) {
        _loadingView = [APPLoadingWindow new];
    }
    return _loadingView;
}

@end
