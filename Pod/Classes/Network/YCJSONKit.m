//
//  NSString+YCJSONKit.m
//  YCPod
//
//  Created by Sheng long on 16/2/2.
//  Copyright © 2016年 shenglong. All rights reserved.
//

#import "YCJSONKit.h"

@implementation NSString (YCJSONKit)

- (id)ObjectFromJSONString_yc{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (!error) {
        if ([jsonObject isKindOfClass:[NSDictionary class]] || [jsonObject isKindOfClass:[NSArray class]]){
            return jsonObject;
        }
    }
#ifdef DEBUG
    NSLog(@"An error happened while deserializing the JSON data.");
#endif
    return nil;
}

@end

@implementation NSData (YCJSONKit)

- (id)ObjectFromJSONData_yc{
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:self options:NSJSONReadingAllowFragments error:&error];
    if (!error) {
        if ([jsonObject isKindOfClass:[NSDictionary class]] || [jsonObject isKindOfClass:[NSArray class]]){
            return jsonObject;
        }
    }
#ifdef DEBUG
    NSLog(@"An error happened while deserializing the JSON data.");
#endif
    return nil;
}

@end

@implementation NSObject (YCJSONKit)

- (NSString *)JSONString_yc{
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (!error) {
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
#ifdef DEBUG
    NSLog(@"An error happened while deserializing the JSON data.");
#endif
    return nil;
}

@end
