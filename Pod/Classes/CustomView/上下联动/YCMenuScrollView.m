//
//  YCMenuScrollView.m
//  WXTest
//
//  Created by SL on 15/1/4.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import "YCMenuScrollView.h"
#import "CommUtls+View.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#define VIEW_TAG(__num)                         (14825+__num)
#define REAL_VIEW_TAG(__num)                    (__num-14825)

typedef UIView* (^SelectView)(id x,NSInteger index,CGRect rect);

@interface YCMenuScrollView()<UIScrollViewDelegate>

@property (nonatomic,strong) YCMenuNavView *navView;

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,copy) SelectView selectViewBlock;

@end

@implementation YCMenuScrollView

- (void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (void)fetchData:(UIView *(^)(id x,NSInteger index,CGRect rect))indexLoc{
    self.selectViewBlock = indexLoc;
    [self showView];
}

- (YCMenuNavView *)fetchNavView{
    YCMenuNavView *navView = [[YCMenuNavView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.navHeight>0?self.navHeight:40)];
    return navView;
}

- (void)showView{
    YCMenuNavView *navView = [self fetchNavView];
    self.navView = navView;
    [self addSubview:navView];
    navView.backgroundColor = [CommUtls colorWithHexString:@"#00a1e9"];
    navView.menuNames = self.nameArray;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(navView.frame), self.frame.size.width, self.frame.size.height-CGRectGetMaxY(navView.frame))];
    self.scrollView = scrollView;
    [scrollView setBounces:NO];
    scrollView.scrollsToTop = NO;
    [scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*navView.menuNames.count, 0)];
    [scrollView setDelegate:self];
    [scrollView setPagingEnabled:YES];
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    [self insertSubview:scrollView belowSubview:navView];
    
    @weakify(self);
    [navView showContent:^(NSInteger index) {
        @strongify(self);
        [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * index, 0) animated:NO];
        [self getShowPage:index];
    }];
    [navView setSelected:self.selectIndex];
}

/**
 *  选中内容获取选中页面
 */
- (void)getShowPage:(NSInteger)index{
    if (self.selectViewBlock) {
        UIView *view = [self.scrollView viewWithTag:VIEW_TAG(index)];
        if (!view) {
            view = self.selectViewBlock(self.nameArray[index],index,CGRectMake(self.scrollView.frame.size.width*index, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height));
            view.tag = VIEW_TAG(index);
            [self.scrollView addSubview:view];
        }
        [CommUtls findView:self belongView:view];
    }
}

- (UIView *)fetchCurrentChildView{
    NSInteger index = self.scrollView.contentOffset.x/self.scrollView.frame.size.width;
    UIView *view = [self.scrollView viewWithTag:VIEW_TAG(index)];
    return view;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger index = scrollView.contentOffset.x/scrollView.frame.size.width;
    [self.navView endDecelerating:index];
}


@end
