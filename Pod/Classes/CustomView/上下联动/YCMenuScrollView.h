//
//  YCMenuScrollView.h
//  WXTest
//
//  Created by SL on 15/1/4.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCMenuNavView.h"

@interface YCMenuScrollView : UIView

/**
 *  显示名称
 */
@property (nonatomic,strong) NSArray *nameArray;

/**
 *  选中位置，第一次可设置默认选中位置，否则默认选中第一个
 */
@property (nonatomic,assign) NSInteger selectIndex;

/**
 *  导航高度
 */
@property (nonatomic,assign) CGFloat navHeight;

/**
 *  获取显示页面
 */
- (void)fetchData:(UIView *(^)(id x,NSInteger index,CGRect rect))indexLoc;

/**
 *  获取当前显示的子页面
 */
- (UIView *)fetchCurrentChildView;

/**
 *  获取顶部View，子类可重写
 */
- (YCMenuNavView *)fetchNavView;

@end
