//
//  YCMenuNavView.m
//  WXTest
//
//  Created by SL on 15/1/4.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import "YCMenuNavView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Masonry/Masonry.h>
#import "CommUtls.h"

#define SELECTED_X_START                50

#define BUTTON_GAP                      0
#define BUTTON_TXT_GAP                  15
#define BUTTON_TXT_FONT                 18

#define BUTTON_TRUE_TAG(__num)          (__num-49128)
#define BUTTON_TAG(__num)               (__num+49128)

#define SELECTED_TXT_COLOR              [UIColor whiteColor]
#define NORMAL_TXT_COLOR                [CommUtls colorWithHexString:@"#b2e5ff"]

typedef void (^IndexLocBlock)(NSInteger index);

@interface YCMenuNavView ()

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,copy) IndexLocBlock locBlock;

@property (nonatomic,assign) NSInteger selectIndex;

@property (nonatomic,strong) UIView *selectView;

@end

@implementation YCMenuNavView

- (void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.normalTextColor = NORMAL_TXT_COLOR;
        self.selectedTextColor = SELECTED_TXT_COLOR;
    }
    return self;
}

- (void)showContent:(void(^)(NSInteger index))indexLoc{
    
    self.locBlock = indexLoc;

    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(BUTTON_GAP, 0, self.frame.size.width-BUTTON_GAP*2, self.frame.size.height)];
    self.scrollView = scrollView;
    scrollView.scrollsToTop = NO;
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    [scrollView setBounces:NO];
    [self addSubview:scrollView];
    
    __block CGFloat xStart = 0;
    
    @weakify(self);
    [self.menuNames enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        @strongify(self);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:obj forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:BUTTON_TXT_FONT]];
        [button setTag:BUTTON_TAG(idx)];
        [button setTitleColor:self.normalTextColor forState:UIControlStateNormal];
        [button sizeToFit];
        button.layer.cornerRadius = 2.5;
        [button setFrame:CGRectMake(xStart, 0, button.frame.size.width+BUTTON_TXT_GAP*2, scrollView.frame.size.height)];
        [scrollView addSubview:button];
        button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            @strongify(self);
            [self setSelected:idx];
            return [RACSignal empty];
        }];
        
        xStart+=(button.frame.size.width+BUTTON_GAP);
    }];
    
    UIView *selectView = [UIView new];
    [scrollView addSubview:selectView];
    selectView.backgroundColor = [CommUtls colorWithHexString:@"#82b6ff"];
    self.selectView = selectView;
    
    [scrollView setContentSize:CGSizeMake(xStart, 0)];
}

- (void)endDecelerating:(NSInteger)index{
    [self setSelected:index];
}

- (void)setSelected:(NSInteger)index{
    //容错
    if (index >= self.menuNames.count) {
        index = 0;
    }
    
    if (self.locBlock) {
        self.locBlock(index);
    }
    
    UIButton *button = [self.scrollView viewWithTag:BUTTON_TAG(self.selectIndex)];
    [button setTitleColor:self.normalTextColor forState:UIControlStateNormal];
    
    self.selectIndex = index;
    UIButton *button1 = [self.scrollView viewWithTag:BUTTON_TAG(self.selectIndex)];
    [button1 setTitleColor:self.selectedTextColor forState:UIControlStateNormal];
    
    self.selectView.frame = CGRectMake(button1.frame.origin.x, self.scrollView.frame.size.height-2, button1.frame.size.width, 2);
    
    [self currentLoc:index];
}

/**
 *  变换菜单显示位置
 */
- (void)currentLoc:(NSInteger)index{
    UIButton *button = [self.scrollView viewWithTag:BUTTON_TAG(self.selectIndex)];
    CGFloat xLoc = button.frame.origin.x;
    if (self.scrollView.contentSize.width>self.scrollView.frame.size.width) {
        if (xLoc<SELECTED_X_START) {
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }else if((xLoc+(self.scrollView.frame.size.width-SELECTED_X_START))>self.scrollView.contentSize.width){
            [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width-self.scrollView.frame.size.width, 0) animated:YES];
        }else{
            [self.scrollView setContentOffset:CGPointMake(xLoc-SELECTED_X_START, 0) animated:YES];
        }
    }
}

@end
