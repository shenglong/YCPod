//
//  YCMenuNavView.h
//  WXTest
//
//  Created by SL on 15/1/4.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCMenuNavView : UIView

/**
 *  群组类别显示字符串数组
 */
@property (nonatomic,strong) NSArray *menuNames;

@property (nonatomic,strong) UIColor *normalTextColor;
@property (nonatomic,strong) UIColor *selectedTextColor;

/**
 *  显示内容
 */
- (void)showContent:(void(^)(NSInteger index))indexLoc;

/**
 *  UIScrollView的代理scrollViewDidEndDecelerating执行时调用
 */
- (void)endDecelerating:(NSInteger)index;

/**
 *  指定选中的位置
 */
- (void)setSelected:(NSInteger)index;

@end
