//
//  DLMenuScrollView.h
//  WXTest
//
//  Created by SL on 15/1/4.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMenuNavView.h"

/**
 *  颜色比较极端，使用时颜色变换有异常
 */

@interface DLMenuScrollView : UIView

/**
 *  菜单栏
 */
@property (nonatomic,readonly) DLMenuNavView *navView;

/**
 *  自定义初始化方法
 *
 *  @param frame         位置
 *  @param nameArray     显示的名称，NSString数组
 *  @param navHeigh      菜单高度
 *  @param normalIndex   默认选中位置
 *  @param normalColor   默认颜色
 *  @param selectedColor 选中颜色
 *  @param font          字体样式
 *  @param currentView   获取当前页面
 *
 *  @return
 */
- (instancetype)initWithFrame:(CGRect)frame
                    nameArray:(NSArray *)nameArray
                    navHeight:(CGFloat)navHeigh
                  normalIndex:(NSInteger)normalIndex
                  normalColor:(UIColor *)normalColor
                selectedColor:(UIColor *)selectedColor
                         font:(UIFont *)font
                  currentView:(UIView *(^)(id x,NSInteger index,CGRect rect))currentView;

/**
 *  设置选中位置
 */
@property (nonatomic,assign) NSInteger selectIndex;

/**
 *  获取当前显示的子页面
 */
- (UIView *)fetchCurrentChildView;

/**
 *  刷新数据源
 *
 *  @param nameArray   显示title
 *  @param selectIndex 选中位置
 */
- (void)reloadData:(NSArray *)nameArray selectIndex:(NSInteger)selectIndex;

/**
 *  获取当前选中位置
 */
@property (nonatomic,assign) NSInteger currentIndex;

@end
