//
//  AppLightView.m
//  Test
//
//  Created by Sheng long on 16/1/28.
//  Copyright © 2016年 Sheng long. All rights reserved.
//

#import "YCVideoLightView.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface YCVideoLightView ()

@property (nonatomic,strong) RACDisposable *timeDis;

@end

@implementation YCVideoLightView

- (void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

+ (instancetype)sharedInstance {
    static YCVideoLightView *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[YCVideoLightView alloc] initWithFrame:CGRectZero];
    });
    return sharedInstance;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
//        NSString *image_url = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"DLVideoPlayer.bundle/%@",@"DLVideo_brightness_bg.png"]];
//        UIImage *image = [UIImage imageWithContentsOfFile:image_url];
        
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"YCVideo_brightness_bg.png"]]
        ;
        [self addSubview:bgView];
        [bgView sizeToFit];
       
        UIView *window = [UIApplication sharedApplication].keyWindow;
        [window addSubview:self];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(window.mas_centerX);
            make.centerY.mas_equalTo(window.mas_centerY);
            make.width.mas_equalTo(bgView.frame.size.width);
            make.height.mas_equalTo(bgView.frame.size.height);
        }];

        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
        
        UIView *wView = nil;
        for (int i = 0 ; i < 16 ; i++) {
            UIView *view = [UIView new];
            view.tag = i+1;
            [bgView addSubview:view];
            view.backgroundColor = [UIColor whiteColor];
            view.alpha = .7;
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(6.5);
                make.height.mas_equalTo(4.5);
                make.bottom.mas_equalTo(-17);
                if (wView) {
                    make.left.mas_equalTo(wView.mas_right).mas_equalTo(1.5);
                }else{
                    make.left.mas_equalTo(14.5);
                }
            }];
            wView = view;
        }
        
        self.hidden = YES;
    }
    return self;
}

+ (void)show{
    [[YCVideoLightView sharedInstance]show];
}

- (void)show{
    self.alpha = 1;
    self.hidden = NO;
    
    CGFloat linght = [UIScreen mainScreen].brightness;
    NSInteger num = [self numOverInt:linght divide:1./16];
    for (int i = 0; i < 16; i++) {
        UIView *view = [self viewWithTag:i+1];
        view.hidden = (num<i+1);
    }
    
    __block YCVideoLightView *blockSelf = self;
    [self.timeDis dispose];
    self.timeDis = [[[[RACSignal interval:1.5 onScheduler:[RACScheduler currentScheduler]] takeUntil:[self rac_willDeallocSignal]] take:1] subscribeNext:^(id x) {
        [UIView animateWithDuration:.5 animations:^{
            blockSelf.alpha = 0;
        } completion:^(BOOL finished) {
            blockSelf.alpha = 1;
            blockSelf.hidden = YES;
        }];
    }];
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue]<8.0 && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight) {
            self.transform = CGAffineTransformMakeRotation(-M_PI_2);
        }else if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft){
            self.transform = CGAffineTransformMakeRotation(M_PI_2);
        }else{
            self.transform = CGAffineTransformIdentity;
        }
    }
}

- (NSInteger)numOverInt:(CGFloat)num
                 divide:(CGFloat)divide{
    if (divide!=0) {
        CGFloat v1 = (CGFloat)(num*1./divide);
        NSInteger v2 = (NSInteger)(num/divide);
        if (v1>v2) {
            return v2+1;
        }
        return v2;
    }
    return num;
}

@end
