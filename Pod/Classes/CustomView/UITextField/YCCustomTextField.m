//
//  CustomTextField.m
//  PhoneApp
//
//  Created by Sheng long on 15-3-7.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "YCCustomTextField.h"
#import <YCPod/CommUtls.h>

@implementation YCCustomTextField

- (void)drawPlaceholderInRect:(CGRect)rect{
    [[self placeholder] drawInRect:rect withAttributes:@{
                                                         NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                         NSForegroundColorAttributeName:(self.placeholderColor?self.placeholderColor:[CommUtls colorWithHexString:@"#cccccc"]),
                                                         }];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds{
    CGSize size = [CommUtls returnContentSize:self.placeholder font:[UIFont systemFontOfSize:13] width:CGFLOAT_MAX];
    CGRect inset = CGRectMake(bounds.origin.x, (bounds.size.height-size.height)/2, bounds.size.width, bounds.size.height);
    return inset;
}

@end
