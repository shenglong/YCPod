//
//  YCSpeedDialView.h
//  Pods
//
//  Created by Sheng long on 16/1/18.
//
//

#import <UIKit/UIKit.h>

@interface YCSpeedDialView : UIView

/**
 *  每行个数
 */
@property (nonatomic,assign) NSInteger lineNum;

/**
 *  每行大小
 */
@property (nonatomic,assign) CGSize itemsSize;

/**
 *  图片名称
 */
@property (nonatomic,strong) NSArray *imageNames;

/**
 *  标题说明
 */
@property (nonatomic,strong) NSArray *titles;

/**
 *  标题样式
 */
@property (nonatomic,strong) UIFont *titlesFont;

/**
 *  标题颜色
 */
@property (nonatomic,strong) UIColor *titlesColor;

/**
 *  选中
 */
- (void)fetchClickItems:(void(^)(NSInteger index))clickIndex;

@end
