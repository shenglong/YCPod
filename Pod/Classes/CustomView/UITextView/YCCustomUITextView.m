//
//  CustomUITextView.m
//  CPACommunity
//
//  Created by Mac on 13-1-15.
//  Copyright (c) 2013年 Mac. All rights reserved.
//

#import "YCCustomUITextView.h"
#import <YCPod/CommUtls.h>
#import <Masonry/Masonry.h>

@interface YCCustomUITextView ()

@property (nonatomic,strong) UILabel *placeHolderLabel;        //提示语言显示Label
@property (nonatomic,strong) NSString *placeholder;            //设置提示语言
@property (nonatomic,strong) UIColor *placeholderColor;        //提示语言颜色
@property (nonatomic,strong) UIFont *placeHolderFont;          //提示语言字体样式

@end

@implementation YCCustomUITextView

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];    
    self.placeholder = nil;
    self.placeholderColor = nil;
    self.placeHolderFont = nil;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if( (self = [super initWithFrame:frame]) ){
        [self customInit];
    }
    return self;
}

- (void)customInit{
    self.placeholderColor = [CommUtls colorWithHexString:@"#cccccc"];
    self.placeHolderFont = [UIFont systemFontOfSize:15];
    [self setPlaceholder:@""];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textChanged:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:nil];
}

- (void)textChanged:(NSNotification *)notification{
    if([[self placeholder] length] == 0){
        return;
    }
    
    if([[self text] length] == 0){
        [[self viewWithTag:999] setAlpha:1];
    }else{
        [[self viewWithTag:999] setAlpha:0];
    }
}

- (void)setText:(NSString *)text{
    [super setText:text];
    [self textChanged:nil];
}

- (void)drawRect:(CGRect)rect{
    [self setTipLabel:self.placeholder];
    [super drawRect:rect];
}

- (void)setTip:(NSString *)tip{
    self.placeholder = tip;
    [self setTipLabel:self.placeholder];
}

/**
 *  设置提示语言
 *
 *  @param tip 
 */
- (void)setTipLabel:(NSString *)tip{
    if( [[self placeholder] length] > 0 ){
        if (_placeHolderLabel == nil ){
            _placeHolderLabel = [UILabel new];
            _placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _placeHolderLabel.numberOfLines = 0;
            _placeHolderLabel.backgroundColor = [UIColor clearColor];
            _placeHolderLabel.textColor = self.placeholderColor;
            _placeHolderLabel.alpha = 0;
            _placeHolderLabel.tag = 999;
            _placeHolderLabel.font = self.placeHolderFont;
            [self addSubview:_placeHolderLabel];
            [_placeHolderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(5);
                make.top.mas_equalTo(7);
                make.right.mas_equalTo(-5);
            }];
        }
    }
    
    _placeHolderLabel.text = self.placeholder;
    [self sendSubviewToBack:_placeHolderLabel];
    
    [self textChanged:nil];
}

@end
