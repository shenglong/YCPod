//
//  YCPreloadScrollView.h
//  TsinghuaSNS
//  预加载的UIScrollView
//  Created by SL on 13-11-12.
//  Copyright (c) 2013年 cdeledu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ScrollPageTip) {
    ScrollViewFirstPage = 10,    //第一页
    ScrollViewLastPage          //最后一页
};

/**
 *  该类是用来将一系列UIView循环显示在UIScrollView中
 *  UIScrollView最多显示3个页面，预加载1+2n个UIView
 */

@interface YCPreloadScrollView : UIView

#pragma mark - required
/**
 *  设置当前显示页面总个数
 */
@property (nonatomic,assign) NSInteger totalNumber;

/**
 *  获取显示页面
 *
 *  @param page 页面位置，已0为起点，
 *
 *  @return view
 */
- (void)fetchShowView:(UIView *(^)(NSInteger page))page;

/**
 *  获取显示页面，子类可重写方法，否则没有显示，有上面方法此方法无效
 *
 *  @param page 页面位置，已0为起点
 *
 *  @return view
 */
- (UIView *)getShowView:(NSInteger)page;

/**
 *  当前显示位置，可设置或者获取
 *  一般第一次使用需要设置此值才能显示页面，执行上面方法之后执行有效
 */
@property (nonatomic,assign) NSInteger curPage;

#pragma mark - optional
/**
 *  主页面
 */
@property (nonatomic,strong) UIScrollView *mainScrollView;

/**
 *  切换显示页面
 *
 *  @param page 切换到的页面位置，以0为起点
 */
- (void)changeShowPage:(void(^)(NSInteger index))page;

/**
 *  是否使用手势滑动，此时会禁用UIScrollView的滑动
 */
@property (nonatomic,assign) BOOL GestureEnabled;

/**
 *  捕获第一页向前翻或者最后一页向后翻事件
 *
 *  @param move block
 */
- (void)getMoveExtremum:(void(^)(ScrollPageTip tip))move;

/**
 *  获取当前显示的页面
 *
 *  @return view
 */
- (UIView *)getCurrentShowView;

@end
