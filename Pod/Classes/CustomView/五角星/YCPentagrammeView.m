//
//  YCPentagrammeView.m
//  SkinApp
//
//  Created by Sheng long on 16/3/3.
//  Copyright © 2016年 Sheng long. All rights reserved.
//

#import "YCPentagrammeView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface YCPentagrammeView ()

@end

@implementation YCPentagrammeView

- (void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",self.class);
#endif
}

- (void)setStarNum:(NSInteger)starNum{
    _starNum = starNum;
    for (int i = 0; i < 5; i++) {
        UIImageView *imageView = [self viewWithTag:10+i];
        if (!imageView) {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.imageSize.width+self.lfGap)*i, 0, self.imageSize.width, self.imageSize.height)];
            imageView.tag = 10+i;
            [self addSubview:imageView];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [self addSubview:button];
            button.frame = imageView.frame;
            @weakify(self);
            button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                @strongify(self);
                self.starNum = i+1;
                return [RACSignal empty];
            }];
        }
        imageView.image = (i<starNum?self.selectedImage:self.normalImage);
    }
}

@end
