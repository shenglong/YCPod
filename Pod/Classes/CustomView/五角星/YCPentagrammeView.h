//
//  YCPentagrammeView.h
//  SkinApp
//
//  Created by Sheng long on 16/3/3.
//  Copyright © 2016年 Sheng long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCPentagrammeView : UIView

/**
 *  五角星大小
 */
@property (nonatomic,assign) CGSize imageSize;

/**
 *  左右间隔
 */
@property (nonatomic,assign) CGFloat lfGap;

/**
 *  默认的五角星图片
 */
@property (nonatomic,strong) UIImage *normalImage;

/**
 *  选中的五角星图片
 */
@property (nonatomic,strong) UIImage *selectedImage;

/**
 *  五角星个数
 */
@property (nonatomic,assign) NSInteger starNum;

@end
