//
//  CustomCollectLayout.m
//  Test
//
//  Created by SL on 15/9/1.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import "AutoCustomCollectLayout.h"

@interface AutoCustomCollectLayout ()

/**
 *  以免滑动的时候对之后对ContentSize有影响，.m添加此属性
 *  用此属性来真正定义是否显示加载更多
 */
@property (nonatomic,assign) BOOL showMore;

@end

@implementation AutoCustomCollectLayout

- (void)dealloc{
#ifdef DEBUG
    NSLog(@"dealloc -- %@",[self class]);
#endif
}

- (CGSize)collectionViewContentSize{
    CGSize size = [super collectionViewContentSize];
    
    //添加加载更多所需
    CGFloat showLoadingMore = 0;
    if (self.showLoadingMore) {
        showLoadingMore = 40;
    }
    
    return CGSizeMake(size.width, showLoadingMore+size.height);
}

@end
