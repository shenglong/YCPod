//
//  CustomCollectLayout.h
//  Test
//
//  Created by SL on 15/9/1.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoCustomCollectLayout : UICollectionViewFlowLayout

/**
 *  显示加载更多
 */
@property (nonatomic,assign) BOOL showLoadingMore;

@end
