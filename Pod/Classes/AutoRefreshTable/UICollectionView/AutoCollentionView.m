//
//  AutoCollentionView.m
//  TenApp
//
//  Created by Sheng long on 15/9/14.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "AutoCollentionView.h"
#import <Masonry/Masonry.h>

@interface AutoCollentionView()

@end

@implementation AutoCollentionView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        if (self.mainTable) {
            [self.mainTable removeFromSuperview];
            self.mainTable = nil;
        }
        
        AutoCustomCollectLayout *layout = [AutoCustomCollectLayout new];
        self.layout = layout;
        
        UICollectionView *collentionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        self.mainTable = collentionView;
        [self addSubview:collentionView];
        collentionView.alwaysBounceVertical = YES;
        collentionView.backgroundColor = [UIColor clearColor];
        collentionView.delegate = self;
        collentionView.dataSource = self;
        
        self.noShowLoadingDone = YES;
        [self setGetDataNumber:20];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {

        if (self.mainTable) {
            [self.mainTable removeFromSuperview];
            self.mainTable = nil;
        }
        
        AutoCustomCollectLayout *layout = [AutoCustomCollectLayout new];
        self.layout = layout;
        
        UICollectionView *collentionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.mainTable = collentionView;
        [self addSubview:collentionView];
        collentionView.alwaysBounceVertical = YES;
        collentionView.backgroundColor = [UIColor clearColor];
        collentionView.delegate = self;
        collentionView.dataSource = self;
        [collentionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
        
        self.noShowLoadingDone = YES;
        [self setGetDataNumber:20];
    }
    return self;
}

@end
