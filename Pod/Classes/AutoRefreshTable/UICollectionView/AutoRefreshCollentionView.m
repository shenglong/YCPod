//
//  AutoRefreshCollentionView.m
//  Test
//
//  Created by Sheng long on 15/9/9.
//  Copyright (c) 2015年 Sheng Long. All rights reserved.
//

#import "AutoRefreshCollentionView.h"
#import "AutoCustomCollectLayout.h"

@interface AutoRefreshCollentionView()

@end

@implementation AutoRefreshCollentionView

- (void)setCurrentRefreshType:(AT_TABEL_REFRESH_STATE)type{
    [super setCurrentRefreshType:type];
    if (type==AT_LOADING_MORE_STATE || type==AT_ALL_REFRESH_STATE) {
        @weakify(self);
        [RACObserve(self.mainTable, contentSize) subscribeNext:^(id x) {
            @strongify(self);
//            self.customLoadingDataView.center = CGPointMake(self.mainTable.frame.size.width/2, self.mainTable.contentSize.height-self.customLoadingDataView.frame.size.height/2);
            self.customLoadingDataView.frame = CGRectMake(0, self.mainTable.contentSize.height-self.customLoadingDataView.frame.size.height, self.mainTable.frame.size.width, self.customLoadingDataView.frame.size.height);
        }];
    }
}

- (void)setAutoDataArray:(NSArray *)autoDataArray{
    [_customLoadingDataView removeFromSuperview];
    [super setAutoDataArray:autoDataArray];
}

- (void)loadingMoreShow{
    if (_customLoadingDataView) {
        @weakify(self);
        AutoCustomCollectLayout *layout = (AutoCustomCollectLayout *)self.mainTable.collectionViewLayout;
        layout.showLoadingMore = NO;
        [[[[RACSignal interval:.1 onScheduler:[RACScheduler currentScheduler]] take:1] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
            @strongify(self);
            [self fetchSignal];
        }];
    }
}

- (void)fetchSignal{
    AutoCustomCollectLayout *layout = (AutoCustomCollectLayout *)self.mainTable.collectionViewLayout;
    //显示加载完成的时候，有时内容高度加上加载完成不过满屏，此时显示加载完成会比较难看
    //通过判断contentSize是否显示出加载完成
    if ((_mainTable.contentSize.height<_mainTable.frame.size.height) || (self.noShowLoadingDone && _customLoadingDataView.currentDataState == AlreadyLoadedState)) {
        [_customLoadingDataView removeFromSuperview];
//    }else if (!_customLoadingDataView.superview){
    }else {
        [_mainTable addSubview:_customLoadingDataView];
        layout.showLoadingMore = YES;
        [_mainTable reloadData];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

@end
