//
//  AutoCollentionView.h
//  TenApp
//
//  Created by Sheng long on 15/9/14.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "AutoRefreshCollentionView.h"
#import "AutoCustomCollectLayout.h"

@interface AutoCollentionView : AutoRefreshCollentionView

@property (nonatomic,strong) AutoCustomCollectLayout *layout;

@end
