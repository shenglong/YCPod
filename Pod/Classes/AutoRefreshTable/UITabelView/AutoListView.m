//
//  AutoListView.m
//  PhoneApp
//
//  Created by Sheng long on 15/4/12.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "AutoListView.h"
#import <Masonry/Masonry.h>
#import "UIView+Loading.h"
#import "APPLoading.h"

@interface AutoListView ()

@property (nonatomic,strong) AutoListViewModel *viewModel;

@end

@implementation AutoListView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        if (self.mainTable) {
            [self.mainTable removeFromSuperview];
            self.mainTable = nil;
        }
        
        UITableView *mainTable = [[UITableView alloc] initWithFrame:self.bounds];
        [self addSubview:mainTable];
        [mainTable setBackgroundColor:[UIColor clearColor]];
        [mainTable setSeparatorColor:[UIColor clearColor]];
        [mainTable setDelegate:self];
        [mainTable setDataSource:self];
        self.mainTable = mainTable;
        
        //默认属性
        self.noShowLoadingDone = YES;
        [self setGetDataNumber:20];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        
        if (self.mainTable) {
            [self.mainTable removeFromSuperview];
            self.mainTable = nil;
        }
        
        UITableView *mainTable = [UITableView new];
        [self addSubview:mainTable];
        [mainTable setBackgroundColor:[UIColor clearColor]];
        [mainTable setSeparatorColor:[UIColor clearColor]];
        [mainTable setDelegate:self];
        [mainTable setDataSource:self];
        [mainTable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.mas_equalTo(0);
        }];
        self.mainTable = mainTable;
        
        //默认属性
        self.noShowLoadingDone = YES;
        [self setGetDataNumber:20];
    }
    return self;
}

- (void)bindSignal{
    
    @weakify(self);
    
    [self getRefreshTableData:^{
        @strongify(self);
        [self.viewModel getData:YES];
    }];
    
    [self getLoadingMoreTableData:^{
        @strongify(self);
        [self.viewModel getData:NO];
    }];
    
    [RACObserve(self.mainTable, contentSize)subscribeNext:^(NSValue *x) {
        @strongify(self);
        self.mainTable.bounces = [x CGSizeValue].height>self.initializeHeight;
    }];
    
    [self.viewModel.updatedContentSignal subscribeNext:^(id x) {
        @strongify(self);
        if ([x integerValue] == Fetch_Data_Done) {
            [self LoadingHideInSelf];
            //只刷新的话可以不用获取真实数据
            self.autoDataArray = [self.viewModel fetchTabelData];
        }
    }];
    
    [self.viewModel.errorSignal subscribeNext:^(id x) {
        @strongify(self);
        if (self.mainTable.bounces) {
            [self recoverShowState];
            [APPLoading LoadingToolTipInWindow:x];
        }else{
            if (self.noRefresh) {
                [self LoadingDoneInSelf:APP_LoadingDone title:x];
            }else{
                [self LoadingCycleInSelf:^{
                    @strongify(self);
                    [self LoadingInSelf];
                    [self.viewModel getData:YES];
                } title:x buttonTitle:nil];
            }
        }
    }];
}

- (void)selctIndex:(void (^)(id vm))vmBlock{
    self.selectBlock = vmBlock;
}

@end
