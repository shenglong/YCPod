//
//  AutoListView.h
//  PhoneApp
//
//  Created by Sheng long on 15/4/12.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "AutoRefreshTableView.h"
#import "AutoListViewModel.h"

typedef void (^SelctIndex)(id vm);

@interface AutoListView : AutoRefreshTableView

/**
 *  没有数据，不需要手动刷新，默认可以刷新数据
 */
@property (nonatomic,assign) BOOL noRefresh;

/**
 *  初始化时UIScrollView的高度，默认为0，以免UICollectionView设置sectionInset时候，影响根据contentSize判断是否显示下拉刷新
 */
@property (nonatomic,assign) CGFloat initializeHeight;

/**
 *  绑定信号
 */
- (void)bindSignal;

/**
 *  设置选中项
 */
@property (nonatomic,copy) SelctIndex selectBlock;

/**
 *  获取选中项
 *
 *  @param vmBlock block
 */
- (void)selctIndex:(void (^)(id vm))vmBlock;

@end
