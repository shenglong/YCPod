//
//  AutoListViewModel.m
//  PhoneApp
//
//  Created by Sheng long on 15/4/12.
//  Copyright (c) 2015年 Sheng long. All rights reserved.
//

#import "AutoListViewModel.h"
#import "AppMacro.h"

@implementation AutoListViewModel

- (void)getData:(BOOL)refresh{
    [self.updatedContentSignal sendNext:@(Fetch_Data_Done)];
}

- (void)dealGetDataSignal:(RACSignal *)signal refresh:(BOOL)refresh{
    @weakify(self);
    RACDisposable *dis = [signal subscribeNext:^(id x) {
        @strongify(self);
        if ([x count]) {
            if (refresh) {
                self.dataArray = x;
            }else{
                self.dataArray = [self.dataArray arrayByAddingObjectsFromArray:x];
            }
            [self.updatedContentSignal sendNext:@(Fetch_Data_Done)];
        }else{
            if(refresh){
                [self.errorSignal sendNext:@"暂无信息"];
            }else{
                [self.updatedContentSignal sendNext:@(Fetch_Data_Done)];
            }
        }
    }error:^(NSError *error) {
        @strongify(self);
        [self.errorSignal sendNext:AppErrorParsing(error)];
    }];
    [self addDisposeSignal:dis];
}

- (NSArray *)fetchTabelData{
    return self.dataArray;
}

@end
