//
//  main.m
//  YCPod
//
//  Created by shenglong on 12/03/2015.
//  Copyright (c) 2015 shenglong. All rights reserved.
//

@import UIKit;
#import "YCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YCAppDelegate class]));
    }
}
