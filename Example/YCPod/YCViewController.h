//
//  YCViewController.h
//  YCPod
//
//  Created by shenglong on 12/03/2015.
//  Copyright (c) 2015 shenglong. All rights reserved.
//

@import UIKit;
//#import <Masonry/Masonry.h>
//#import <ReactiveCocoa/ReactiveCocoa.h>
//#import "CommUtls.h"

/*
 
 更新下pod，上传所有信息
 YCPod.podspec文件里面的标签需要一致
 
 git tag -m "first release" "0.3.3"
 git push --tags
 
 pod lib lint --verbose --allow-warnings --no-clean
 
 pod trunk push YCPod.podspec --verbose --allow-warnings
 
 //打包静态库
 //验证
 pod lib lint YCPod.podspec --only-errors --verbose
 //打包，--library生成.a
 pod package YCPod.podspec --library --force
 
 pod update --verbose --no-repo-update
 
 pod install --verbose --no-repo-update
 
 pod 'YCPod', :podspec => '/Users/SL/Desktop/SL/Lib/YCPod/YCPod.podspec'
 
*/

@interface YCViewController : UIViewController


@end



//s.subspec 'View' do |view|
//#view.public_header_files = 'Pod/Classes/View/**/*.h'
//#view.resources = "Pod/Assets/View/*.png"
//view.source_files = 'Pod/Classes/View/YCViewConstant.{h,m}'
//view.dependency 'YCPod/CommUtls'
//view.dependency 'Masonry', '~> 0.6.2'
//view.dependency 'ReactiveCocoa', '~> 2.5'
//
//view.subspec 'Loading' do |loading|
//loading.public_header_files = 'Pod/Classes/View/Loading/**/*.h'
//#loading.source_files = 'Pod/Classes/View/Loading/**/*'
//loading.resources = "Pod/Assets/View/*.png"
//loading.source_files = [
//                        'Pod/Classes/View/Loading/LLARingSpinnerView/*.{h,m}',
//#'Pod/Classes/View/Loading/UIView/*.{h,m}',
//                        ]
//end
//end



