//
//  YCAppDelegate.h
//  YCPod
//
//  Created by shenglong on 12/03/2015.
//  Copyright (c) 2015 shenglong. All rights reserved.
//

@import UIKit;

@interface YCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
